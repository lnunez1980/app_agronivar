package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 06/11/2017.
 */

public class UsuarioA {
    private long lgId;
    private String IdCliente;
    private String factura;
    private String tipodoc;
    private String fechafact;
    private String montof;


    public UsuarioA(long lgId, String idCliente, String factura, String tipodoc, String fechafact, String montof) {
        this.lgId = lgId;
        IdCliente = idCliente;
        this.factura = factura;
        this.tipodoc = tipodoc;
        this.fechafact = fechafact;
        this.montof = montof;
    }

    public long getLgId() {
        return lgId;
    }

    public void setLgId(long lgId) {
        this.lgId = lgId;
    }

    public String getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(String idCliente) {
        IdCliente = idCliente;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getFechafact() {
        return fechafact;
    }

    public void setFechafact(String fechafact) {
        this.fechafact = fechafact;
    }

    public String getMontof() {
        return montof;
    }

    public void setMontof(String montof) {
        this.montof = montof;
    }
}
