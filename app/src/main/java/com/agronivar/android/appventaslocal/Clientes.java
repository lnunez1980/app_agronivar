package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 11/10/2017.
 */

public class Clientes {
    private long IdCliente = 0;
    private String Nombre = "";
    private String RIF = "";
    private String Direccion = "";
    private String Vendedor = "";
    private String Cupo = "";
    private String Canal = "";
    private String CondPago = "";
    private String Barrio = "";
    private String Lista = "";
    private String Plazo = "";
    private String Activo = "";

    public Clientes(long idCliente, String nombre, String RIF, String direccion, String vendedor, String cupo, String canal, String condPago, String barrio, String lista, String plazo, String activo) {
        IdCliente = idCliente;
        Nombre = nombre;
        this.RIF = RIF;
        Direccion = direccion;
        Vendedor = vendedor;
        Cupo = cupo;
        Canal = canal;
        CondPago = condPago;
        Barrio = barrio;
        Lista = lista;
        Plazo = plazo;
        Activo = activo;
    }

    public long getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(long idCliente) {
        IdCliente = idCliente;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getRIF() {
        return RIF;
    }

    public void setRIF(String RIF) {
        this.RIF = RIF;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getVendedor() {
        return Vendedor;
    }

    public void setVendedor(String vendedor) {
        Vendedor = vendedor;
    }

    public String getCupo() {
        return Cupo;
    }

    public void setCupo(String cupo) {
        Cupo = cupo;
    }

    public String getCanal() {
        return Canal;
    }

    public void setCanal(String canal) {
        Canal = canal;
    }

    public String getCondPago() {
        return CondPago;
    }

    public void setCondPago(String condPago) {
        CondPago = condPago;
    }

    public String getBarrio() {
        return Barrio;
    }

    public void setBarrio(String barrio) {
        Barrio = barrio;
    }

    public String getLista() {
        return Lista;
    }

    public void setLista(String lista) {
        Lista = lista;
    }

    public String getPlazo() {
        return Plazo;
    }

    public void setPlazo(String plazo) {
        Plazo = plazo;
    }

    public String getActivo() {
        return Activo;
    }

    public void setActivo(String activo) {
        Activo = activo;
    }
}

