package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 16/11/2017.
 */

public class pagosFacturas {
    private String idpago;
    private String fecha;
    private long idVendedor;
    private long IdCliente;
    private long factura;
    private String documento;
    private String fechaP;
    private String tipopago;
    private double monto;
    private String bancoe;
    private String bancor;
    private String deposito;

    public pagosFacturas(String idpago, String fecha, long idVendedor, long idCliente, long factura, String documento, String fechaP, String tipopago, double monto, String bancoe, String bancor, String deposito) {
        this.idpago = idpago;
        this.fecha = fecha;
        this.idVendedor = idVendedor;
        IdCliente = idCliente;
        this.factura = factura;
        this.documento = documento;
        this.fechaP = fechaP;
        this.tipopago = tipopago;
        this.monto = monto;
        this.bancoe = bancoe;
        this.bancor = bancor;
        this.deposito = deposito;
    }

    public String getIdpago() {
        return idpago;
    }

    public void setIdpago(String idpago) {
        this.idpago = idpago;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public long getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(long idVendedor) {
        this.idVendedor = idVendedor;
    }

    public long getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(long idCliente) {
        IdCliente = idCliente;
    }

    public long getFactura() {
        return factura;
    }

    public void setFactura(long factura) {
        this.factura = factura;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFechaP() {
        return fechaP;
    }

    public void setFechaP(String fechaP) {
        this.fechaP = fechaP;
    }

    public String getTipopago() {
        return tipopago;
    }

    public void setTipopago(String tipopago) {
        this.tipopago = tipopago;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getBancoe() {
        return bancoe;
    }

    public void setBancoe(String bancoe) {
        this.bancoe = bancoe;
    }

    public String getBancor() {
        return bancor;
    }

    public void setBancor(String bancor) {
        this.bancor = bancor;
    }

    public String getDeposito() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito = deposito;
    }
}
