package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 20/11/2017.
 */

public class ItemAnticipos {
    public String idanticipo;
    public String idcliente;
    public String Fechap;
    public String tipoPago;
    public Double Monto;

    public ItemAnticipos(String idanticipo, String idcliente, String fechap, String tipoPago, Double monto) {
        this.idanticipo = idanticipo;
        this.idcliente = idcliente;
        Fechap = fechap;
        this.tipoPago = tipoPago;
        Monto = monto;
    }

    public String getIdanticipo() {
        return idanticipo;
    }

    public void setIdanticipo(String idanticipo) {
        this.idanticipo = idanticipo;
    }

    public String getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(String idcliente) {
        this.idcliente = idcliente;
    }

    public String getFechap() {
        return Fechap;
    }

    public void setFechap(String fechap) {
        Fechap = fechap;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public Double getMonto() {
        return Monto;
    }

    public void setMonto(Double monto) {
        Monto = monto;
    }
}
