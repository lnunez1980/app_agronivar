package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 17/10/2017.
 */

public class ItemClientes {
    public int Estatus;
    public String Nombre;
    public String Codigo;

    public ItemClientes(int estatus, String nombre, String codigo) {
        Estatus = estatus;
        Nombre = nombre;
        Codigo = codigo;
    }

    public int getEstatus() {
        return Estatus;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getCodigo() {
        return Codigo;
    }
}
