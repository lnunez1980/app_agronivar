package com.agronivar.android.appventaslocal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lnuñez on 05/10/2017.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "ventas.db";
    private static final int DB_VERSION = 1;

    public DbHelper(Context context) {
        super(context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbManager.tb_cliente);
        db.execSQL(DbManager.tb_usuario);
        db.execSQL(DbManager.tb_usuarioa);
        db.execSQL(DbManager.tb_cobranza);
        db.execSQL(DbManager.tb_pagosFact);
        db.execSQL(DbManager.tb_anticipos);
        db.execSQL(DbManager.tb_bancos);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS clientes");
        db.execSQL("DROP TABLE IF EXISTS usuario");
        db.execSQL("DROP TABLE IF EXISTS UsuarioActivo");
        db.execSQL("DROP TABLE IF EXISTS cobranza");
        db.execSQL("DROP TABLE IF EXISTS pagosFact");
        db.execSQL("DROP TABLE IF EXISTS Anticipos");
        db.execSQL("DROP TABLE IF EXISTS bancos");
        onCreate(db);
    }
}
