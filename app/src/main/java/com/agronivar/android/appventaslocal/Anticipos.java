package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 11/12/2017.
 */

public class Anticipos {

    private String idanticipo;
    private String fecha;
    private Long idVendedor;
    private Long idCliente;
    private String documento;
    private String fechav;
    private String tipopago;
    private Double monto;
    private String bancoe;
    private String bancor;
    private String deposito;

    public Anticipos(String idanticipo, String fecha, Long idVendedor, Long idCliente, String documento, String fechav, String tipopago, Double monto, String bancoe, String bancor, String deposito) {
        this.idanticipo = idanticipo;
        this.fecha = fecha;
        this.idVendedor = idVendedor;
        this.idCliente = idCliente;
        this.documento = documento;
        this.fechav = fechav;
        this.tipopago = tipopago;
        this.monto = monto;
        this.bancoe = bancoe;
        this.bancor = bancor;
        this.deposito = deposito;
    }

    public String getIdanticipo() {
        return idanticipo;
    }

    public void setIdanticipo(String idanticipo) {
        this.idanticipo = idanticipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Long getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Long idVendedor) {
        this.idVendedor = idVendedor;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFechav() {
        return fechav;
    }

    public void setFechav(String fechav) {
        this.fechav = fechav;
    }

    public String getTipopago() {
        return tipopago;
    }

    public void setTipopago(String tipopago) {
        this.tipopago = tipopago;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getBancoe() {
        return bancoe;
    }

    public void setBancoe(String bancoe) {
        this.bancoe = bancoe;
    }

    public String getBancor() {
        return bancor;
    }

    public void setBancor(String bancor) {
        this.bancor = bancor;
    }

    public String getDeposito() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito = deposito;
    }
}
