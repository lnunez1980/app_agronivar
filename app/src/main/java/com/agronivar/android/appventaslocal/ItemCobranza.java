package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 17/10/2017.
 */

public class ItemCobranza {
    public int Estatus;
    public String Fechaf;
    public String Fechav;
    public Long Codigo;
    public Double Monto;
    public Long Factura;
    public Double Montoc;
    public Double Montor;
    public Double Montoa;
    public Double Montocr;

    public ItemCobranza(int estatus, String fechaf, String fechav, Long codigo, Double monto, Long factura, Double montoc, Double montor, Double montoa, Double montocr) {
        Estatus = estatus;
        Fechaf = fechaf;
        Fechav = fechav;
        Codigo = codigo;
        Monto = monto;
        Factura = factura;
        Montoc = montoc;
        Montor = montor;
        Montoa = montoa;
        Montocr = montocr;
    }

    public int getEstatus() {
        return Estatus;
    }

    public void setEstatus(int estatus) {
        Estatus = estatus;
    }

    public String getFechaf() {
        return Fechaf;
    }

    public void setFechaf(String fechaf) {
        Fechaf = fechaf;
    }

    public String getFechav() {
        return Fechav;
    }

    public void setFechav(String fechav) {
        Fechav = fechav;
    }

    public Long getCodigo() {
        return Codigo;
    }

    public void setCodigo(Long codigo) {
        Codigo = codigo;
    }

    public Double getMonto() {
        return Monto;
    }

    public void setMonto(Double monto) {
        Monto = monto;
    }

    public Long getFactura() {
        return Factura;
    }

    public void setFactura(Long factura) {
        Factura = factura;
    }

    public Double getMontoc() {
        return Montoc;
    }

    public void setMontoc(Double montoc) {
        Montoc = montoc;
    }

    public Double getMontor() {
        return Montor;
    }

    public void setMontor(Double montor) {
        Montor = montor;
    }

    public Double getMontoa() {
        return Montoa;
    }

    public void setMontoa(Double montoa) {
        Montoa = montoa;
    }

    public Double getMontocr() {
        return Montocr;
    }

    public void setMontocr(Double montocr) {
        Montocr = montocr;
    }
}
