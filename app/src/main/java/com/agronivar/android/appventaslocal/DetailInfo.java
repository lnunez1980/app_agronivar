package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 11/01/2018.
 */

public class DetailInfo {

    private String idpago = "";
    private String Factura = "";
    private String IdCliente = "";
    private String nombCliente = "";
    private String montot = "";


    public String getIdpago() {
        return idpago;
    }

    public void setIdpago(String idpago) {
        this.idpago = idpago;
    }

    public String getFactura() {
        return Factura;
    }

    public void setFactura(String factura) {
        Factura = factura;
    }

    public String getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(String idCliente) {
        IdCliente = idCliente;
    }

    public String getNombCliente() {
        return nombCliente;
    }

    public void setNombCliente(String nombCliente) {
        this.nombCliente = nombCliente;
    }

    public String getMontot() {
        return montot;
    }

    public void setMontot(String montot) {
        this.montot = montot;
    }
}