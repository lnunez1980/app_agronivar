package com.agronivar.android.appventaslocal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Cobranzas extends AppCompatActivity {
    private ListView list;
    ArrayList<Cobranza> lisC;
    ArrayList<UsuarioA> lisU;
    public ItemCobranza datos[];
    public TextView cod;
    public TextView montof;
    String nfac[];
    double pagosxfactura;
    double resta;
    double anticipo;
    double notacr;
    @Override
    protected void onStart() {
        super.onStart();

        DbManager manager = new DbManager(this);

        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        lisC = manager.cargarCursorCobranza(this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"","'RV','DD'");
        ItemCobranza datos2[] =new ItemCobranza[lisC.size()];

        for(int x=0;x<lisC.size();x++) {
            pagosxfactura=0;
            resta=0;
            notacr = manager.NotasCredito(this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"", lisC.get(x).getFactura()+"");
            anticipo = manager.Anticipos(this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"", lisC.get(x).getFactura()+"");
            pagosxfactura = manager.PagosxFactura(Cobranzas.this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"", lisC.get(x).getFactura()+"");
            resta = lisC.get(x).getMonto()-pagosxfactura - anticipo - notacr;
            datos2[x] = new ItemCobranza(R.drawable.fact,lisC.get(x).getFechafact(),lisC.get(x).getFechavenc(),lisC.get(x).getIdCliente(),
                    lisC.get(x).getMonto(),lisC.get(x).getFactura(),pagosxfactura,resta,anticipo,notacr);
        }

        datos=datos2;
        AdaptadorCobranzas adaptador = new AdaptadorCobranzas(this,datos);
        list.setAdapter(adaptador);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cobranzas);
        list = (ListView) findViewById(R.id.listaCobranza);
        DbManager manager = new DbManager(this);

        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        lisC = manager.cargarCursorCobranza(this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"","'RV','DD'");
        ItemCobranza datos2[] =new ItemCobranza[lisC.size()];

        for(int x=0;x<lisC.size();x++) {

            pagosxfactura=0;
            resta=0;
            notacr = manager.NotasCredito(this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"", lisC.get(x).getFactura()+"");
            anticipo = manager.Anticipos(this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"", lisC.get(x).getFactura()+"");
            pagosxfactura = manager.PagosxFactura(Cobranzas.this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"", lisC.get(x).getFactura()+"");
            resta = lisC.get(x).getMonto()-pagosxfactura - notacr;

            datos2[x] = new ItemCobranza(R.drawable.fact,lisC.get(x).getFechafact(),lisC.get(x).getFechavenc(),
                    lisC.get(x).getIdCliente(),lisC.get(x).getMonto(),lisC.get(x).getFactura(),pagosxfactura,resta,anticipo,notacr);
        }

        datos=datos2;
        AdaptadorCobranzas adaptador = new AdaptadorCobranzas(this,datos);
        list.setAdapter(adaptador);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                cod = (TextView) view.findViewById(R.id.IdFactura);
                nfac = cod.getText().toString().split(":");
                montof = (TextView) view.findViewById(R.id.montof);
                DbManager manager = new DbManager(getApplicationContext());
                manager.UpdateCursorUsuarioA(getApplicationContext(),lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"",nfac[1].trim()," "," ",montof.getText().toString());
                Intent k = new Intent(getApplicationContext(), pagosF.class);
                startActivity(k);
            }
        });
    }


    class AdaptadorCobranzas extends ArrayAdapter<ItemCobranza> {
        Locale locale = new Locale("es","VE");
        NumberFormat nf = NumberFormat.getNumberInstance(locale);

        public AdaptadorCobranzas(Context context, ItemCobranza[] cobranzas) {
            super(context, R.layout.itemlistcobranza,cobranzas);
        }
        public View getView(int pos, View convertView, ViewGroup parent){

            if (nf instanceof DecimalFormat) {
                ((DecimalFormat)nf).setDecimalSeparatorAlwaysShown(true);
                ((DecimalFormat)nf).setMinimumFractionDigits(2);
            }

            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.itemlistcobranza,null);

            TextView Fechas = (TextView)item.findViewById(R.id.Fechas);
            Fechas.setText("Fecha V.:" + datos[pos].getFechav());

            TextView Codigo = (TextView)item.findViewById(R.id.idCliente);
            Codigo.setText("Cliente: "+datos[pos].getCodigo());

            TextView Monto = (TextView)item.findViewById(R.id.montof);
            Monto.setText("Facturado: "+nf.format(datos[pos].getMonto()));

            TextView Montoc = (TextView)item.findViewById(R.id.montoc);
            Montoc.setText("Cancelado: "+nf.format(datos[pos].getMontoc()));

            TextView Montodb = (TextView)item.findViewById(R.id.montoa);
            Montodb.setText("Abono: "+nf.format(datos[pos].getMontoa()));

            TextView Montor = (TextView)item.findViewById(R.id.montor);
            Montor.setText("Resta: "+nf.format(datos[pos].getMontor()));

            TextView Factura = (TextView)item.findViewById(R.id.IdFactura);
            Factura.setText("Factura: "+datos[pos].getFactura());

            ImageView imagen =(ImageView)item.findViewById(R.id.estatus);
            imagen.setImageResource(datos[pos].getEstatus());

            return (item);
        }
    }
}
