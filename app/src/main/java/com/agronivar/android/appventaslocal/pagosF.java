package com.agronivar.android.appventaslocal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class pagosF extends AppCompatActivity {
    private static final String TAG ="" ;
    TextView documento;
    TextView monto;
    TextView txtFechaV;
    TextView txttpago;
    TextView txtresta;
    TextView idpago;
    ArrayList<UsuarioA> lisU;
    ArrayList<pagosFacturas> lisC;
    ArrayList<Bancos> lisBE;
    ArrayList<Bancos> lisBR;
    private ListView lista;
    public ItemPagos datos[];
    public Bancos bancosE[];
    public Bancos bancosR[];
    String tipopago;
    String mfac[];
    String idp[];
    private String[] arraySpinner;
    private int dia, mes, ano;
    Button btnfecha;
    Button btnagregar;
    Button btncancelar;
    Button btnMenu;
    double mont;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    Double pagosxfactura=0.00;
    Spinner bancoe;
    Spinner bancor;
    LayoutInflater flaterE;
    LayoutInflater flaterR;
    String Codbancoe;
    String Codbancor;
    Boolean band = true;
    Locale locale;
    NumberFormat nf;
    double anticipos;
    double notacr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagos_f2);

        // configuracion de Decimales en variavle de formato numerico

        locale = new Locale("es","VE");
        nf = NumberFormat.getNumberInstance(locale);

        if (nf instanceof DecimalFormat) {
            ((DecimalFormat)nf).setDecimalSeparatorAlwaysShown(true);
            ((DecimalFormat)nf).setMinimumFractionDigits(2);
        }

        // Menu Forma de pago

        tipopago = "Efectivo";
        btnMenu = (Button) findViewById(R.id.btnMenu);
        registerForContextMenu(btnMenu);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openContextMenu(btnMenu);
            }
        });

        documento = (TextView) findViewById(R.id.documento);
        documento.setEnabled(false);
        monto = (TextView) findViewById(R.id.montoa);
        txtFechaV = (TextView) findViewById(R.id.txtFechaV);
        txtresta = (TextView) findViewById(R.id.txtresta);
        txttpago = (TextView) findViewById(R.id.txttpago);
        txttpago.setText("Efectivo");
        lista = (ListView) findViewById(R.id.listap);
        bancoe = (Spinner) findViewById(R.id.bancoe);
        bancor = (Spinner) findViewById(R.id.bancor);
        bancoe.setEnabled(false);
        bancor.setEnabled(false);

        bancoe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView code = (TextView) view.findViewById(R.id.idCliente);
                Codbancoe = code.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bancor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView codr = (TextView) view.findViewById(R.id.idCliente);
                Codbancor = codr.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Item Clic Lista Pagos
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView , final View view, int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(pagosF.this);

                builder.setTitle("Eliminar Pagos");
                builder.setMessage("Desea Elimianr el Pago Seleccionado?");


                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        idpago = (TextView) view.findViewById(R.id.idCliente);
                        idp = idpago.getText().toString().split(" ");
                        idpago.setText(idp[1]);

                        DbManager manager = new DbManager(getApplicationContext());
                        manager.eliminarPago(idpago.getText().toString());
                        MostrarPagos();
                        saldofactura();
                        dialog.dismiss();
                    }

                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        // Cargar Listas Bancos
        CargarBancos();

        //Asignar Fecha Actual
        Date myDate = new Date();
        txtFechaV.setText(new SimpleDateFormat("dd-MM-yyyy").format(myDate));

        // Menu Eliminar PAgo
        registerForContextMenu(lista);

        saldofactura();

        // Mostrar Pago de factura en la lista
        MostrarPagos();

        // Boton Mostrar Popup Fecha
        btnfecha = (Button) findViewById(R.id.btnfecha);
        btnfecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int ano = cal.get(Calendar.YEAR);
                int mes = cal.get(Calendar.MONTH);
                int dia = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        pagosF.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        ano,mes,dia);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                String fecha;
                        mes = mes + 1;
                fecha = dia + "-" + mes + "-" + ano;
                if (mes < 10){
                    fecha = dia + "-" + "0"+mes + "-" + ano;
                }
                if (dia < 10){
                    fecha = "0" + dia + "-" + mes + "-" + ano;
                }
                txtFechaV.setText(fecha);
            }
        };

        // Boton Agregar Pago
        btnagregar = (Button) findViewById(R.id.btnagregar);
        btnagregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbManager manager = new DbManager(getApplicationContext());
                band = true;
                if(tipopago.trim().isEmpty()){
                    band =false;
                }
                if(monto.getText().toString().trim().isEmpty()){
                    band =false;
                }
                if((documento.getText().toString().trim().isEmpty()) && (tipopago != "Efectivo")){
                    band =false;
                }
                if ((bancoe.getSelectedItemPosition()==0) && (tipopago != "Efectivo")){
                    band =false;
                    if ((bancoe.getSelectedItemPosition()==0) && (tipopago == "Deposito")){
                        band =true;
                    }
                }
                if ((bancor.getSelectedItemPosition()==0)&& (tipopago != "Efectivo")){
                    band =false;
                    if ((bancor.getSelectedItemPosition()==0) && (tipopago == "Cheque")){
                        band = true;
                    }
                }
                if(band==true){
                    // Pagos por factura
                    pagosxfactura = manager.PagosxFactura(pagosF.this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente(),lisU.get(0).getFactura());
                    anticipos = manager.Anticipos(pagosF.this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"",lisU.get(0).getFactura()+"");
                    pagosxfactura = pagosxfactura + anticipos;
                    monto.setText(monto.getText().toString().replace(",","."));
                    Double VerificarMonto = Double.valueOf(monto.getText().toString());
                    if((VerificarMonto <= mont) && (VerificarMonto != 0 ))   {
                            InsertarMostrarPagos();
                        ressetcampos();
                    }else if((mont - pagosxfactura)<=0){
                        Toast.makeText(getApplicationContext(), "Factura no tiene Saldo", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Monto superior al saldo de la factura", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // Eventos de Menu Contextual Forma de pago
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.listpagos){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_elipago,menu);
        }

        if(v.getId() == R.id.btnMenu){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_tpago,menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.menuEfectivo:
                tipopago = "Efectivo";
                txttpago.setText(tipopago);
                documento.setEnabled(false);
                bancoe.setEnabled(false);
                bancor.setEnabled(false);
                break;
            case  R.id.menuCheque:
                tipopago = "Cheque";
                txttpago.setText(tipopago);
                documento.setEnabled(true);
                bancoe.setEnabled(true);
                bancor.setEnabled(true);
                break;
            case  R.id.menuDeposito:
                tipopago = "Deposito";
                txttpago.setText(tipopago);
                documento.setEnabled(true);
                bancoe.setEnabled(false);
                bancor.setEnabled(true);
                break;
            case  R.id.menuTransferencia:
                tipopago = "Transferencia";
                txttpago.setText(tipopago);
                documento.setEnabled(true);
                bancoe.setEnabled(true);
                bancor.setEnabled(true);
                break;
            case  R.id.menuEliminar:
                break;

        }
        return super.onContextItemSelected(item);
    }

    public static String covertirmonto(double mont){
        return new DecimalFormat("#.##########################").format(mont);
    }

    public static String ajustaMoneda(String valor1){

        int lastPosComas = -1;
        int lastPosPuntos = -1;

        lastPosComas = valor1.lastIndexOf(",");
        lastPosPuntos = valor1.lastIndexOf(".");

        if (lastPosComas > lastPosPuntos)
        {
            valor1 = valor1.replace(".", "");
            valor1 = valor1.replace(",", ".");
        }
        else
        {
            valor1 = valor1.replace(",", "");
        }

        Double valorDouble = new Double(valor1);
        System.out.println(valorDouble);

        return valor1;
    }

    private void InsertarMostrarPagos(){

        //Funcion para llenar lista de pagos por factura


        DbManager manager = new DbManager(getApplicationContext());
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        BigDecimal monc = new BigDecimal(monto.getText().toString());
        manager.insertarPagosFacturas(lisU.get(0).getLgId(),Long.parseLong(lisU.get(0).getIdCliente()),Long.parseLong(lisU.get(0).getFactura())
                ,documento.getText().toString(),tipopago, Double.parseDouble(monc+""),txtFechaV.getText().toString(),Codbancoe,Codbancor);

        saldofactura();

        lisC = manager.cargarCursorPagosfact(getApplicationContext(),lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"",lisU.get(0).getFactura());
        ItemPagos datos2[] = new ItemPagos[lisC.size()];
        for(int x=0;x<lisC.size();x++) {
            datos2[x] = new ItemPagos(lisC.get(x).getFechaP(), lisC.get(x).getTipopago(),lisC.get(x).getFactura(), (double) lisC.get(x).getMonto(),lisC.get(x).getIdpago(),"",lisC.get(x).getDocumento() );
        }
        datos=datos2;
        AdaptadorPagos adaptador = new AdaptadorPagos(getApplicationContext(),datos);
        lista.setAdapter(adaptador);

    }
    private void ressetcampos(){
        Date myDate = new Date();
        band = true;
        txtFechaV.setText(new SimpleDateFormat("dd-MM-yyyy").format(myDate));
        txttpago.setText("Efectivo");
        documento.setEnabled(false);
        bancoe.setSelection(0);
        bancor.setSelection(0);
        documento.setText("");


    }
    private void MostrarPagos(){

        //Funcion para llenar lista de pagos por factura

        DbManager manager = new DbManager(getApplicationContext());
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        lisC = manager.cargarCursorPagosfact(getApplicationContext(),lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"",lisU.get(0).getFactura());
        ItemPagos datos2[] = new ItemPagos[lisC.size()];
        for(int x=0;x<lisC.size();x++) {
            datos2[x] = new ItemPagos(lisC.get(x).getFechaP(), lisC.get(x).getTipopago(),lisC.get(x).getFactura(), (double) lisC.get(x).getMonto(),lisC.get(x).getIdpago(),"" ,lisC.get(x).getDocumento());
        }
        datos=datos2;
        AdaptadorPagos adaptador = new AdaptadorPagos(getApplicationContext(),datos);
        lista.setAdapter(adaptador);
    }

    private void saldofactura(){
        // Pagos por factura
        DbManager manager = new DbManager(getApplicationContext());
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        pagosxfactura = manager.PagosxFactura(this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente(),lisU.get(0).getFactura());
        anticipos = manager.Anticipos(pagosF.this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"", lisU.get(0).getFactura()+"");
        notacr = manager.NotasCredito (pagosF.this,lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"", lisU.get(0).getFactura()+"");
        pagosxfactura = pagosxfactura + anticipos + notacr;
        // Traer monto factura
        mfac =lisU.get(0).getMontof().split(" ");
        mfac[1] = mfac[1].replace(".","");
        mfac[1] = mfac[1].replace(",",".");
        BigDecimal montb = new BigDecimal(mfac[1]);
        mont = Double.parseDouble(montb+"") - pagosxfactura;
        monto.setText(covertirmonto(mont));
        // Restante
        txtresta.setText(nf.format(mont)+" Bs.");
    }

    private void CargarBancos(){
        int Cant;
        //Funcion para llenar lista de Bancos Emisor
        DbManager manager = new DbManager(pagosF.this);
        lisBE = manager.cargarCursorBancos(pagosF.this,"E");
        Cant = lisBE.size();
        Bancos datosE[] = new Bancos[Cant];

        for(int x=0;x<Cant;x++) {
            datosE[x] = new Bancos(lisBE.get(x).getIdbanco(),lisBE.get(x).getNombre(),lisBE.get(x).getTipo());
        }
        bancosE=datosE;
        AdaptadorBancosE adaptadorE = new AdaptadorBancosE(pagosF.this,bancosE);
        bancoe.setAdapter(adaptadorE);

        //Funcion para llenar lista de Bancos Receptor
        lisBR = manager.cargarCursorBancos(pagosF.this,"R");
        Cant = lisBR.size();
        Bancos datosR[] = new Bancos[Cant];
        for(int x=0;x<Cant;x++) {
            datosR[x] = new Bancos(lisBR.get(x).getIdbanco(),lisBR.get(x).getNombre(),lisBR.get(x).getTipo());
        }
        bancosR=datosR;
        AdaptadorBancosR adaptadorR = new AdaptadorBancosR(pagosF.this,bancosR);
        bancor.setAdapter(adaptadorR);
    }

    class AdaptadorBancosR extends BaseAdapter {
        Activity activity;
        Bancos[] bancos;
        LayoutInflater inflater;

        //Clase Adaptador para la Lista de Bancos

        public AdaptadorBancosR(Activity activity, Bancos[] bancos) {
                this.activity = activity;
                this.bancos = bancos;
                inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return bancos.length;
        }

        @Nullable
        @Override
        public Bancos getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View Row = inflater.inflate(R.layout.itemlistbancos,null);
            TextView Idbanco = (TextView)Row.findViewById(R.id.idCliente);
            Idbanco.setText(bancosR[i].getIdbanco());

            TextView Nombre = (TextView)Row.findViewById(R.id.Nombre);
            Nombre.setText(bancosR[i].getNombre());

            return Row;
        }
    }

    class AdaptadorBancosE extends BaseAdapter {
        Activity activity;
        Bancos[] bancos;
        LayoutInflater inflater;

        //Clase Adaptador para la Lista de Bancos

        public AdaptadorBancosE(Activity activity, Bancos[] bancos) {
            this.activity = activity;
            this.bancos = bancos;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return bancos.length;
        }

        @Nullable
        @Override
        public Bancos getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View Row = inflater.inflate(R.layout.itemlistbancos,null);
            TextView Idbanco = (TextView)Row.findViewById(R.id.idCliente);
            Idbanco.setText(bancosE[i].getIdbanco());

            TextView Nombre = (TextView)Row.findViewById(R.id.Nombre);
            Nombre.setText(bancosE[i].getNombre());

            return Row;
        }
    }


    class AdaptadorPagos extends ArrayAdapter<ItemPagos> {

        //Clase Adaptador para la Lista de pagos

        Locale locale = new Locale("es","VE");
        NumberFormat nf = NumberFormat.getNumberInstance(locale);

        public AdaptadorPagos(Context context, ItemPagos[] pagos) {
            super(context, R.layout.itemlistpagos, pagos);
        }
        public View getView(int pos, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.itemlistpagos,null);

            TextView Fechav = (TextView)item.findViewById(R.id.fechav);
            Fechav.setText("F. Valor: " + datos[pos].getFechap());

            TextView Monto = (TextView)item.findViewById(R.id.montoa);
            Monto.setText(nf.format(datos[pos].getMonto()));

            TextView Factura = (TextView)item.findViewById(R.id.IdFactura);
            Factura.setText("Fact: "+datos[pos].getCodigof());

            TextView tipoP = (TextView)item.findViewById(R.id.tipopago);
            tipoP.setText(datos[pos].getTipoPago());

            TextView idpago = (TextView)item.findViewById(R.id.idCliente);
            idpago.setText("Cod: " + datos[pos].getIdpago());

            return (item);
        }
    }


}
