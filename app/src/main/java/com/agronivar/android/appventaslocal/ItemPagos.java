package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 20/11/2017.
 */

public class ItemPagos {
    public String Fechap;
    public String tipoPago;
    public long Codigof;
    public Double Monto;
    public String Idpago;
    public String nombreCliente;
    public String documento;

    public ItemPagos(String fechap, String tipoPago, long codigof, Double monto, String idpago, String nombreCliente, String documento) {
        Fechap = fechap;
        this.tipoPago = tipoPago;
        Codigof = codigof;
        Monto = monto;
        Idpago = idpago;
        this.nombreCliente = nombreCliente;
        this.documento = documento;
    }

    public String getFechap() {
        return Fechap;
    }

    public void setFechap(String fechap) {
        Fechap = fechap;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public long getCodigof() {
        return Codigof;
    }

    public void setCodigof(long codigof) {
        Codigof = codigof;
    }

    public Double getMonto() {
        return Monto;
    }

    public void setMonto(Double monto) {
        Monto = monto;
    }

    public String getIdpago() {
        return Idpago;
    }

    public void setIdpago(String idpago) {
        Idpago = idpago;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }
}
