package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 18/10/2017.
 */

public class Usuario {

    private long lgId;
    private String lgpass;
    private String Nombre;

    public Usuario(long lgId, String lgpass, String nombre) {
        this.lgId = lgId;
        this.lgpass = lgpass;
        Nombre = nombre;
    }

    public long getLgId() {
        return lgId;
    }

    public void setLgId(long lgId) {
        this.lgId = lgId;
    }

    public String getLgpass() {
        return lgpass;
    }

    public void setLgpass(String lgpass) {
        this.lgpass = lgpass;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
