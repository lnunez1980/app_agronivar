package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 11/01/2018.
 */

import java.util.ArrayList;

public class HeaderInfo {

    private String descripcion;
    private String montot;
    private ArrayList<DetailInfo> pagosList = new ArrayList<DetailInfo>();

    public String getMontot() {
        return montot;
    }
    public void setMontot(String montot) {
        this.montot = montot;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public ArrayList<DetailInfo> getPagosList() {
        return pagosList;
    }
    public void setPagosList(ArrayList<DetailInfo> pagosListp) {
        this.pagosList = pagosListp;
    }
}