package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 26/10/2017.
 */

public class Cobranza {

	private long idVendedor;
    private long IdCliente;
    private long factura;
    private String tipodoc;
    private String fechafact;
    private Double monto;
    private String fechavenc;

    public Cobranza(long idVendedor, long idCliente, long factura, String tipodoc, String fechafact, Double monto, String fechavenc) {
        this.idVendedor = idVendedor;
        IdCliente = idCliente;
        this.factura = factura;
        this.tipodoc = tipodoc;
        this.fechafact = fechafact;
        this.monto = monto;
        this.fechavenc = fechavenc;
    }

    public long getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(long idVendedor) {
        this.idVendedor = idVendedor;
    }

    public long getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(long idCliente) {
        IdCliente = idCliente;
    }

    public long getFactura() {
        return factura;
    }

    public void setFactura(long factura) {
        this.factura = factura;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getFechafact() {
        return fechafact;
    }

    public void setFechafact(String fechafact) {
        this.fechafact = fechafact;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getFechavenc() {
        return fechavenc;
    }

    public void setFechavenc(String fechavenc) {
        this.fechavenc = fechavenc;
    }
}
