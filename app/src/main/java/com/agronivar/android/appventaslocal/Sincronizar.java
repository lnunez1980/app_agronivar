package com.agronivar.android.appventaslocal;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Sincronizar extends AppCompatActivity {
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;
    String[] ids;
    private ListView list;
    ArrayList<UsuarioA> lisU;
    ArrayList<pagosFacturas> lisC;
    ArrayList<Anticipos> lisA;
    Button btndescargar;
    Button btnActualizar;
    Button btnCrearArchivo;
    static String tbotones;
    String[] values = new String[] { "Android List View"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sincronizar);

        list = (ListView) findViewById(R.id.ListaArchivos);

        btnCrearArchivo = (Button) findViewById(R.id.btnCrearArchivo);
        btnCrearArchivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CrearArchivo();
            }
        });

        btndescargar = (Button) findViewById(R.id.btndescargar);
        btndescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDownload();
            }
        });

        btnActualizar = (Button)findViewById(R.id.btnActualizar);
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bleer;
                DbManager manager = new DbManager(getApplicationContext());
                manager.borrartablas("clientes");
                manager.borrartablas("UsuarioActivo");
                manager.borrartablas("Cobranza");
                manager.borrartablas("pagosFact");
                manager.borrartablas("anticipos");
                manager.borrartablas("bancos");
                lisU = manager.cargarCursorUsuarioA(getApplicationContext());
                try {
                    String TraceSinc[] = new String[4];
                    bleer = leerArchivo("TEMCLIENTES");
                    TraceSinc[0] = new String("Clientes: Listo");
                    bleer = leerArchivo("TEMPOCARTERA");
                    TraceSinc[1] = new String("Cobranzas: Listo");
                    bleer = leerArchivo("TEMASESOR");
                    TraceSinc[2] = new String("Asesores: Listo");
                    bleer = leerArchivo("TEMPOBANCO");
                    TraceSinc[3] = new String("Bancos: Listo");

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, TraceSinc);
                    list.setAdapter(adapter);
                    if (bleer == 1){
                        Toast.makeText(getApplicationContext(),"Proceso Terminado", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Error descarge archivos nuevamente...", Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        if (tbotones.equals("INT")){
            btndescargar.setEnabled(false);
            btnActualizar.setEnabled(false);
            btnCrearArchivo.setEnabled(true);
        }else{
            btndescargar.setEnabled(true);
            btnActualizar.setEnabled(true);
            btnCrearArchivo.setEnabled(false);
        }
    }

    public int CrearArchivo()
    {
        Date myDate = new Date();
        String fecha = (new SimpleDateFormat("dd-MM-yyyy").format(myDate));
        DbManager manager = new DbManager(this);
        lisU = manager.cargarCursorUsuarioA(this);
        String Sp = ";";
        final File path = getFilesDir();
        int nPagos, nClientes, nFacturas;
        String tEscenario="", linea="", nombVend="",documentoCH="",documento="", ref="";
        String monto;
        String tipoPago;
        if(!path.exists())
        {
            path.mkdirs();
        }

        final File file = new File(path, lisU.get(0).getLgId()+".txt");
        try
        {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

            lisC = manager.cargarCursorPagos(this,lisU.get(0).getLgId()+"",fecha);
            lisA = manager.ListaAnticipos(this,lisU.get(0).getLgId()+"",fecha);

            nPagos = manager.NumeroPagos(this,lisU.get(0).getLgId()+"");
            nClientes = manager.NumeroClientes(this,lisU.get(0).getLgId()+"");
            nFacturas = manager.NumeroFacturas(this,lisU.get(0).getLgId()+"");
            nombVend = manager.NombreVendedor(this,lisU.get(0).getLgId()+"");

            if(nPagos > 1 && nClientes > 1 && nFacturas > 1 ){
                tEscenario = "NPNFNC";
            }

            if(nPagos == 1 && nClientes >= 1 && nFacturas >= 1 ){
                tEscenario = "NPNFNC";
            }

            for(int x=0;x<lisC.size();x++) {
                monto = covertirmonto(lisC.get(x).getMonto());
                documentoCH = lisC.get(x).getDocumento();
                documento = lisC.get(x).getDocumento();
                tipoPago = lisC.get(x).getTipopago();
                if(tipoPago.equals("Cheque")){
                    documentoCH = lisC.get(x).getDocumento();
                    documento = lisC.get(x).getDeposito();
                    ref = "C-" + lisC.get(x).getIdCliente() + " F-" + lisC.get(x).getFactura() + " CH-" +documentoCH + " B-" + manager.NombreBanco(this,lisC.get(x).getBancoe());
                }else{
                    documentoCH = "";
                    documento = lisC.get(x).getDocumento();
                    if(tipoPago.equals("Deposito")){
                        ref = "C-" + lisC.get(x).getIdCliente() + " F-" + lisC.get(x).getFactura() + " DP-" +documento + " B-" + manager.NombreBanco(this,lisC.get(x).getBancor());
                    }
                    if(tipoPago.equals("Transferencia")){
                        ref = "C-" + lisC.get(x).getIdCliente() + " F-" + lisC.get(x).getFactura() + " TX-" +documento + " B-" + manager.NombreBanco(this,lisC.get(x).getBancor());
                    }
                }
                if(tipoPago.equals("Efectivo")){
                    documentoCH = "S/N";
                    documento = "S/N";
                    ref = "Efectivo";
                }
                if (lisC.get(x).getBancor().toString().equals("R000")){
                    Toast.makeText(getApplicationContext(), "Verifique Banco Receptor en Cobranza", Toast.LENGTH_LONG).show();
                    return 0;
                }
                linea = (lisC.get(x).getIdpago()+ Sp +tEscenario+ Sp + lisC.get(x).getIdCliente()+ Sp + lisC.get(x).getIdVendedor() + Sp +
                        nombVend+ Sp + lisC.get(x).getFactura()+ Sp + lisC.get(x).getTipopago()+ Sp + lisC.get(x).getBancoe() + Sp + lisC.get(x).getBancor() + Sp +
                        (monto+"") + Sp + "DZ" + Sp + documentoCH + Sp + documento + Sp + lisC.get(x).getFecha() + Sp + lisC.get(x).getFechaP() + Sp + ref ) + "\r\n";
                myOutWriter.append(linea) ;
            }

            for(int x=0;x<lisA.size();x++) {
                monto = covertirmonto(lisA.get(x).getMonto());
                documentoCH = lisA.get(x).getDocumento();
                documento = lisA.get(x).getDocumento();
                tipoPago = lisA.get(x).getTipopago();
                if(tipoPago.equals("Cheque")){
                    documentoCH = lisA.get(x).getDocumento();
                    documento = lisA.get(x).getDeposito();
                    ref = "C-" + lisA.get(x).getIdCliente() + " CH-" +documentoCH + " B-" + manager.NombreBanco(this,lisA.get(x).getBancoe());
                }else{
                    documentoCH = "";
                    documento = lisA.get(x).getDocumento();
                    if(tipoPago.equals("Deposito")){
                        ref = "C-" + lisA.get(x).getIdCliente() + " DP-" +documento + " B-" + manager.NombreBanco(this,lisA.get(x).getBancor());
                    }
                    if(tipoPago.equals("Transferencia")){
                        ref = "C-" + lisA.get(x).getIdCliente() + " TX-" +documento + " B-" + manager.NombreBanco(this,lisA.get(x).getBancor());
                    }
                }
                if(tipoPago.equals("Efectivo")){
                    documentoCH = "S/N";
                    documento = "S/N";
                    ref = "Efectivo";
                }
                if (lisA.get(x).getBancor().toString().equals("R000")){
                    Toast.makeText(getApplicationContext(), "Verifique Banco Receptor en Cobranza", Toast.LENGTH_LONG).show();
                    return 0;
                }
                linea = (lisA.get(x).getIdanticipo()+ Sp +"Anticipo"+ Sp + lisA.get(x).getIdCliente()+ Sp + lisA.get(x).getIdVendedor() + Sp +
                        nombVend+ Sp + ""+ Sp + lisA.get(x).getTipopago()+ Sp + lisA.get(x).getBancoe() + Sp + lisA.get(x).getBancor() + Sp +
                        ((monto+"").replace(",",".")) + Sp + "DZ" + Sp + documentoCH + Sp + documento + Sp + lisA.get(x).getFecha() + Sp + lisA.get(x).getFechav() + Sp + ref ) + "\r\n";
                myOutWriter.append(linea) ;
            }
            if ((lisC.size()+lisA.size())==0){
                Toast.makeText(getApplicationContext(), "No hay datos para Sincronizar...", Toast.LENGTH_LONG).show();
                return 0;
            }
            myOutWriter.close();
            fOut.flush();
            fOut.close();
            //Toast.makeText(getApplicationContext(), "Archivo Creado...", Toast.LENGTH_SHORT).show();
            subirArchivo();
        }
        catch (IOException e)
        {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        return 1;
    }
    public static String eliminarCaracteresEspeciales(String input) {
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i=0; i<original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output;
    }
    public static String covertirmonto(double mont){
        return new DecimalFormat("#.##########################").format(mont);
    }
    public int leerArchivo(String filename) throws IOException {

        File dir = getFilesDir();
        File file = new File(dir,"/"+filename+".txt");
        DbManager manager = new DbManager(this);


        if(file.exists())
        {
            StringBuilder text = new StringBuilder();

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            Boolean band = false;
            while ((line = br.readLine()) != null) {
                line = eliminarCaracteresEspeciales(line);
                line = line.replaceAll(";;","; ;");
                ids = line.split(";");
                if(filename == "TEMCLIENTES"){
                   manager.insertar(ids[0],ids[1],ids[2],ids[3],ids[4],ids[5],ids[6],ids[7],ids[8],ids[9],ids[10],ids[11]);
                   band=true;
                }
                if(filename == "TEMASESOR"){
                    manager.insertarUsuario(Long.parseLong(ids[0]),"",ids[1]);
                    band=true;
                }
                if(filename == "TEMPOCARTERA"){
                    if (isValidInteger(ids[0].toString())) {
                        if (isValidInteger(ids[1].toString())) {
                            manager.insertarCobranza(Long.parseLong(ids[0]),Long.parseLong(ids[1]),Long.parseLong(ids[2]),ids[3],ids[4],ids[5],ids[6],ids[7],ids[8]);
                            band=true;
                        }
                    }
                }
                if(filename == "TEMPOBANCO"){
                    if (ids[2].equals("E")){
                        manager.insertarbancos(ids[0],ids[1],ids[2]);
                        band=true;
                    }
                    if (ids[2].equals("R") && ((ids[0].equals("ME620")) || (ids[0].equals("CA889"))
                            || (ids[0].equals("FC490")) || (ids[0].equals("BN158")) || (ids[0].equals("OD637"))
                            || (ids[0].equals("PR830")) || (ids[0].equals("NC825")) || (ids[0].equals("EX646"))
                            || (ids[0].equals("BB540")) || (ids[0].equals("BC822")) || (ids[0].equals("PL484"))
                            || (ids[0].equals("VZ356")) || (ids[0].equals("CI037")) ) ){
                        manager.insertarbancos(ids[0],ids[1],ids[2]);
                        band=true;
                    }
                }
            }
            if (band==true){
                Intent k = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(k);
                finish();
            }
        }else{
            Toast.makeText(this, "Archivo no Existe", Toast.LENGTH_SHORT).show();
            return 0;
        }
        return 1;
    }

    public static Boolean isValidInteger(String value) {
        try {
            Integer val = Integer.valueOf(value);
            if (val != null)
                return true;
            else
                return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    private void startDownload() {
        String url;
        url="http://www.agronivar.com/ServidorWeb/TEMCLIENTES.txt";
        new DownloadFileAsync().execute(url,"TEMCLIENTES.txt");

        url="http://www.agronivar.com/ServidorWeb/TEMPOCARTERA.txt";
        new DownloadFileAsync().execute(url,"TEMPOCARTERA.txt");

        url="http://www.agronivar.com/ServidorWeb/TEMASESOR.txt";
        new DownloadFileAsync().execute(url,"TEMASESOR.txt");

        url="http://www.agronivar.com/ServidorWeb/TEMPOBANCO.txt";
        new DownloadFileAsync().execute(url,"TEMPOBANCO.txt");

    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Descargando..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override

        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }
        @Override
        protected String doInBackground( String... aurl) {
            int count;
            try {
                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                File file = getFilesDir();
                OutputStream output = new FileOutputStream(getFilesDir()+"/"+aurl[1]);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {}
            return null;
        }
        protected void onProgressUpdate(String... progress) {
            mProgressDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }
    }


    public void subirArchivo() throws SocketException, UnknownHostException, IOException {

        try {

            FTPClient ftpClient = new FTPClient();
            if (Build.VERSION.SDK_INT>9);
            {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            ftpClient.connect("www.agronivar.com",21);
            ftpClient.login("apkcobranza@agronivar.com", "apk+2017");
            DbManager manager = new DbManager(this);
            lisU = manager.cargarCursorUsuarioA(this);



            boolean directoryExists = ftpClient.changeWorkingDirectory("/" + lisU.get(0).getLgId() + "");
            if(directoryExists == true){
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                BufferedInputStream buffIn=null;
                File dir = getFilesDir();
                File file = new File(dir,"/"+lisU.get(0).getLgId()+".txt");
                buffIn=new BufferedInputStream(new FileInputStream(file.toString()));
                ftpClient.enterLocalPassiveMode();
                ftpClient.storeFile(lisU.get(0).getLgId()+".txt", buffIn);
                ftpClient.retrieveFileStream("/" + lisU.get(0).getLgId() + lisU.get(0).getLgId()+".txt");
                int returnCode  = ftpClient.getReplyCode();
                if (returnCode == 550){
                    Toast.makeText(getApplicationContext(), "Archivo Enviado Satisfactoriamente...", Toast.LENGTH_SHORT).show();
                    manager.borrartablas("clientes");
                    manager.borrartablas("UsuarioActivo");
                    manager.borrartablas("Cobranza");
                    manager.borrartablas("pagosFact");
                    manager.borrartablas("anticipos");
                    manager.borrartablas("bancos");
                }else if (returnCode == 500){
                    Toast.makeText(getApplicationContext(), "Archivo no se pudo enviar...", Toast.LENGTH_SHORT).show();
                }
                buffIn.close();
                ftpClient.logout();
                ftpClient.disconnect();
            }else{
                ftpClient.makeDirectory("/" + lisU.get(0).getLgId() + "");
                ftpClient.changeWorkingDirectory("/" + lisU.get(0).getLgId() + "");
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                BufferedInputStream buffIn=null;
                File dir = getFilesDir();
                File file = new File(dir,"/"+lisU.get(0).getLgId()+".txt");
                buffIn=new BufferedInputStream(new FileInputStream(file.toString()));
                ftpClient.enterLocalPassiveMode();
                ftpClient.storeFile(lisU.get(0).getLgId()+".txt", buffIn);
                ftpClient.retrieveFileStream("/" + lisU.get(0).getLgId() + lisU.get(0).getLgId()+".txt");
                int returnCode  = ftpClient.getReplyCode();
                if (returnCode == 550){
                    Toast.makeText(getApplicationContext(), "Archivo Enviado Satisfactoriamente...", Toast.LENGTH_SHORT).show();
                    manager.borrartablas("clientes");
                    manager.borrartablas("UsuarioActivo");
                    manager.borrartablas("Cobranza");
                    manager.borrartablas("pagosFact");
                    manager.borrartablas("anticipos");
                    manager.borrartablas("bancos");
                }else if (returnCode == 500){
                    Toast.makeText(getApplicationContext(), "Archivo no se pudo enviar...", Toast.LENGTH_SHORT).show();
                }                buffIn.close();
                ftpClient.logout();
                ftpClient.disconnect();
            }

        } catch (Exception e){
            Log.i("consola","error");}
    }

    public static void Botones(String tipo){
        tbotones=tipo;
    }

}
