package com.agronivar.android.appventaslocal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by lnuñez on 05/10/2017.
 */

public class DbManager {
    public static final String TABLE_NAME = "clientes";
    public static final String TABLE_NAMEu = "usuario";
    public static final String TABLE_NAMEua = "UsuarioActivo";
    public static final String TABLE_NAMEc = "Cobranza";
    public static final String TABLE_NAMEpf = "pagosFact";
    public static final String TABLE_NAMEac = "anticipos";
    public static final String TABLE_NAMEb = "bancos";

    public static final String tb_cliente = "CREATE TABLE Clientes (_id INTEGER NOT NULL," +
            "Nombre	TEXT, RIF TEXT, Direccion TEXT, Vendedor TEXT, Cupo	TEXT, Canal TEXT, CondPago TEXT, Barrio TEXT, Lista TEXT, Plazo TEXT, Activo TEXT, PRIMARY KEY(_id,Vendedor));";

    public static final String tb_usuario = "CREATE TABLE Usuario (lgId INTEGER, lgpass TEXT, Nombre TEXT, PRIMARY KEY(lgId));";

    public static final String tb_cobranza ="CREATE TABLE Cobranza (idVendedor INTEGER NOT NULL, idCliente INTEGER NOT NULL,"+
            " factura NUMERIC NOT NULL, tipodoc TEXT NOT NULL, fechafact DATE NOT NULL, monto DOUBLE, fechavenc DATE, RefFact NUMERIC, IndCME TEXT, PRIMARY KEY(idVendedor,idCliente,factura,tipodoc,fechafact));";

    public static final String tb_usuarioa = "CREATE TABLE UsuarioActivo ( lgId INTEGER, IdCliente TEXT, factura TEXT, tipodoc TEXT, fechafact DATE, montof REAL, PRIMARY KEY(lgId));";

    public static final String tb_pagosFact = "CREATE TABLE pagosFact (idpago TEXT, fecha DATE NOT NULL , idVendedor INTEGER NOT NULL, idCliente INTEGER NOT NULL, factura NUMERIC NOT NULL, "+
            "documento TEXT NOT NULL, fechap DATE NOT NULL, tipopago TEXT, monto DOUBLE, bancoe TEXT, bancor TEXT, deposito TEXT, PRIMARY KEY (idpago,fecha,idVendedor,idCliente,factura,documento,fechap));";

    public static final String tb_anticipos = "CREATE TABLE Anticipos (idanticipo TEXT, fecha DATE NOT NULL , idVendedor INTEGER NOT NULL, idCliente INTEGER NOT NULL, "+
            "documento TEXT NOT NULL, fechav DATE NOT NULL, tipopago TEXT, monto DOUBLE, bancoe TEXT, bancor TEXT, deposito TEXT, PRIMARY KEY (idanticipo,fecha,idVendedor,idCliente,documento,fechav));";

    public static final String tb_bancos = "CREATE TABLE bancos (Idbanco TEXT, Nombre TEXT, Tipo TEXT, PRIMARY KEY(Idbanco));";

    private DbHelper helper;
    private SQLiteDatabase db;

    public DbManager(Context context) {
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();

    }

    public void insertar(String IdCliente,String Nombre, String RIF, String Direccion, String Vendedor, String Cupo, String Canal,
                     String CondPago, String Barrio, String Lista, String Plazo, String Activo){
        ContentValues valores = new ContentValues();

        valores.put("_id",IdCliente);
        valores.put("Nombre",Nombre);
        valores.put("RIF",RIF);
        valores.put("Direccion",Direccion);
        valores.put("Vendedor",Vendedor);
        valores.put("Cupo",Cupo);
        valores.put("Canal",Canal);
        valores.put("CondPago",CondPago);
        valores.put("Barrio",Barrio);
        valores.put("Lista",Lista);
        valores.put("Plazo",Plazo);
        valores.put("Activo",Activo);

        db.insert(TABLE_NAME,null,valores);

    }

    public void insertarUsuarioA(long lgId){
        db.execSQL("Delete from UsuarioActivo;");
        ContentValues valores = new ContentValues();
        valores.put("lgId",lgId);
        valores.put("IdCliente"," ");
        valores.put("factura"," ");
        valores.put("tipodoc"," ");
        valores.put("fechafact"," ");
        valores.put("montof"," ");

        db.insert(TABLE_NAMEua,null,valores);

    }

    public void insertarbancos(String Idbanco, String Nombre, String Tipo){
        ContentValues valores = new ContentValues();

        valores.put("Idbanco",Idbanco);
        valores.put("Nombre",Nombre);
        valores.put("Tipo",Tipo);


        db.insert(TABLE_NAMEb,null,valores);

    }

    public void insertarUsuario(long lgId, String lgpass, String Nombre){
        ContentValues valores = new ContentValues();

        valores.put("lgId",lgId);
        valores.put("lgpass",lgpass);
        valores.put("Nombre",Nombre);


        db.insert(TABLE_NAMEu,null,valores);

    }

    public void eliminarPago(String idpago) {

        String where = "idpago = '" + idpago + "'";
        db.delete(TABLE_NAMEpf, where, null);

    }

    public void eliminarAnticipo(String idAnticipo) {

        String where = "idanticipo = '" + idAnticipo + "'";
        db.delete(TABLE_NAMEac, where, null);

    }

    public void updateContraseñaUsuario(long lgId, String lgpass) {

        String where = "lgId = '" + lgId + "'";
        ContentValues valores = new ContentValues();

        valores.put("lgpass",lgpass);
        db.update(TABLE_NAMEu, valores, where, null);
        db.close();
    }
    public void borrartablas(String table){
        db = helper.getWritableDatabase();
        db.delete(table,null,null);
        db.close();
    }

    public void insertarPagosFacturas(long idVendedor, long IdCliente, long factura,String documento, String tipopago, double monto, String fechap, String bancoe, String bancor){
        ContentValues valores = new ContentValues();
        String idpago = "";
        String fecha ="";
        Date myDate = new Date();

        idpago = (new SimpleDateFormat("yyyyMMddhhmmss").format(myDate));
        fecha = (new SimpleDateFormat("dd-MM-yyyy").format(myDate));;

        valores.put("idpago",idpago);
        valores.put("fecha",fecha);

        valores.put("idVendedor",idVendedor);
        valores.put("IdCliente",IdCliente);
        valores.put("factura",factura);
        valores.put("documento",documento);
        valores.put("tipopago", tipopago);
        valores.put("monto",monto);
        valores.put("fechap",fechap);
        valores.put("bancoe",bancoe);
        valores.put("bancor",bancor);
        valores.put("deposito",documento);


        db.insert(TABLE_NAMEpf,null,valores);

    }

    public void insertarAnticipos(long idVendedor, long IdCliente, String documento, String tipopago, double monto, String fechav, String bancoe, String bancor){
        ContentValues valores = new ContentValues();
        String idanticipo = "";
        String fecha ="";
        Date myDate = new Date();

        idanticipo = (new SimpleDateFormat("yyyyMMddhhmmss").format(myDate));
        fecha = (new SimpleDateFormat("dd-MM-yyyy").format(myDate));;

        valores.put("idanticipo",idanticipo);
        valores.put("fecha",fecha);

        valores.put("idVendedor",idVendedor);
        valores.put("IdCliente",IdCliente);
        valores.put("documento",documento);
        valores.put("tipopago", tipopago);
        valores.put("monto",monto);
        valores.put("fechav",fechav);
        valores.put("bancoe",bancoe);
        valores.put("bancor",bancor);
        valores.put("deposito",documento);
        db.insert(TABLE_NAMEac,null,valores);

    }

    public void insertarCobranza(long idVendedor, long IdCliente, long factura,String tipodoc, String fechafact, String monto, String fechavenc, String RefFact, String IndCME){
        ContentValues valores = new ContentValues();

         valores.put("idVendedor",idVendedor);
        valores.put("IdCliente",IdCliente);
        valores.put("factura",factura);
        valores.put("tipodoc",tipodoc);
        valores.put("fechafact", String.valueOf(fechafact));
        valores.put("monto",monto);
        valores.put("fechavenc", String.valueOf(fechavenc));
        valores.put("RefFact",RefFact);
        valores.put("IndCME",IndCME);

        db.insert(TABLE_NAMEc,null,valores);

    }

    public  ArrayList<pagosFacturas> cargarCursorPagosfact(Context context, String IdVendedor, String IdCliente, String factura){
        db = helper.getReadableDatabase();
        ArrayList<pagosFacturas> lis = new ArrayList<pagosFacturas>();
        Cursor c = db.rawQuery("SELECT * FROM pagosFact where IdVendedor = "+ IdVendedor +" and idCliente = "+ IdCliente +" and factura = "+ factura +";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new pagosFacturas(c.getString(0),c.getString(1),Long.parseLong(c.getString(2)),Long.parseLong(c.getString(3)),Long.parseLong(c.getString(4)),
                        c.getString(5),c.getString(6),c.getString(7), Double.parseDouble(c.getString(8)),c.getString(9),c.getString(10),c.getString(11)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public  ArrayList<pagosFacturas> cargarCursorAnticiposCH(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        ArrayList<pagosFacturas> lis = new ArrayList<pagosFacturas>();
        Cursor c = db.rawQuery("SELECT * FROM Anticipos where tipopago = 'Cheque' and IdVendedor = "+ IdVendedor +";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new pagosFacturas(c.getString(0),c.getString(1),
                        Long.parseLong(c.getString(2)),Long.parseLong(c.getString(3)),0,
                        c.getString(4),c.getString(5),c.getString(6),
                        Double.parseDouble(c.getString(7)),c.getString(8),c.getString(9),c.getString(10)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public  ArrayList<pagosFacturas> cargarCursorPagosfactCH(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        ArrayList<pagosFacturas> lis = new ArrayList<pagosFacturas>();
        Cursor c = db.rawQuery("SELECT * FROM pagosFact where tipopago = 'Cheque' and IdVendedor = "+ IdVendedor +";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new pagosFacturas(c.getString(0),c.getString(1),Long.parseLong(c.getString(2)),Long.parseLong(c.getString(3)),Long.parseLong(c.getString(4)),
                        c.getString(5),c.getString(6),c.getString(7), Double.parseDouble(c.getString(8)),c.getString(9),c.getString(10),c.getString(11)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public  ArrayList<pagosFacturas> cargarCursorPagos(Context context, String IdVendedor, String fecha){
        db = helper.getReadableDatabase();
        ArrayList<pagosFacturas> lis = new ArrayList<pagosFacturas>();
        Cursor c = db.rawQuery("SELECT * FROM pagosFact where IdVendedor = "+ IdVendedor + " and fecha = '"+ fecha +"';",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new pagosFacturas(c.getString(0),c.getString(1),Long.parseLong(c.getString(2)),Long.parseLong(c.getString(3)),Long.parseLong(c.getString(4)),
                        c.getString(5),c.getString(6),c.getString(7), Double.parseDouble(c.getString(8)),c.getString(9),c.getString(10),c.getString(11)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public  ArrayList<Anticipos> ListaAnticipos(Context context, String IdVendedor, String fecha){
        db = helper.getReadableDatabase();
        ArrayList<Anticipos> lis = new ArrayList<Anticipos>();
        Cursor c = db.rawQuery("SELECT * FROM Anticipos where IdVendedor = "+ IdVendedor +" and fecha = '"+ fecha +"';",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Anticipos(c.getString(0),c.getString(1),Long.parseLong(c.getString(2)),Long.parseLong(c.getString(3)),
                        c.getString(4),c.getString(5),c.getString(6), Double.parseDouble(c.getString(7)),c.getString(8),c.getString(9),c.getString(10)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public  ArrayList<Anticipos> cargarCursorAnticipos(Context context, String IdVendedor, String IdCliente){
        db = helper.getReadableDatabase();
        ArrayList<Anticipos> lis = new ArrayList<Anticipos>();
        Cursor c = db.rawQuery("SELECT * FROM Anticipos where IdVendedor = "+ IdVendedor +" and idCliente = "+ IdCliente +";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Anticipos(c.getString(0),c.getString(1),Long.parseLong(c.getString(2)),Long.parseLong(c.getString(3)),
                        c.getString(4),c.getString(5),c.getString(6), Double.parseDouble(c.getString(7)),c.getString(8),c.getString(9),c.getString(10)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public  String NombreBanco(Context context, String IdBanco){
        db = helper.getReadableDatabase();
        String Nomb = "";
        Cursor c = db.rawQuery("SELECT Nombre FROM bancos where Idbanco = '"+IdBanco+"';",null);
        if (c.moveToFirst()) {
            do {
                Nomb = c.getString(0);
            } while (c.moveToNext());
        }
        return Nomb;
    }

    public  String NombreCliente(Context context, String IdCliente){
        db = helper.getReadableDatabase();
        String Nomb = "";
        Cursor c = db.rawQuery("SELECT Nombre FROM clientes where _Id = "+IdCliente+";",null);
        if (c.moveToFirst()) {
            do {
                Nomb = c.getString(0);
            } while (c.moveToNext());
        }
        return Nomb;
    }

    public  String GetBancor(Context context, String IdVendedor, String Idpago){
        db = helper.getReadableDatabase();
        String bancor = "";
        Cursor c = db.rawQuery("SELECT bancor FROM pagosFact where IdVendedor = '" + IdVendedor + "'" + " and idpago = '" + Idpago + "';",null);
        if (c.getCount()==0){
            Cursor a = db.rawQuery("SELECT bancor FROM Anticipos where IdVendedor = '" + IdVendedor + "'" + " and idanticipo = '" + Idpago + "';",null);
            c=a;
        }

        if (c.moveToFirst()) {
            do {
                bancor = c.getString(0);
            } while (c.moveToNext());
        }
        return bancor;
    }

    public  String GetDeposito(Context context, String IdVendedor, String Idpago){
        db = helper.getReadableDatabase();
        String nDeposito = "";
        Cursor c = db.rawQuery("SELECT deposito FROM pagosFact where IdVendedor = '" + IdVendedor + "'" + " and idpago = '" + Idpago + "';",null);
        if (c.getCount()==0){
            Cursor a = db.rawQuery("SELECT deposito FROM Anticipos where IdVendedor = '" + IdVendedor + "'" + " and idanticipo = '" + Idpago + "';",null);
            c=a;
        }

        if (c.moveToFirst()) {
            do {
                nDeposito = c.getString(0);
            } while (c.moveToNext());
        }
        return nDeposito;
    }

    public  String NombreVendedor(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        String Nomb = "";
        Cursor c = db.rawQuery("SELECT Nombre FROM usuario where lgid = "+IdVendedor+";",null);
        if (c.moveToFirst()) {
            do {
                Nomb = c.getString(0);
            } while (c.moveToNext());
        }
        return Nomb;
    }

    public  double TotalPagosxTipo(Context context, String IdVendedor, String Tipopago, String fecha){
        db = helper.getReadableDatabase();
        double mont = 0.00;
        Cursor c = db.rawQuery("SELECT Sum(monto) as Total FROM pagosFact where IdVendedor = "+ IdVendedor +" and tipopago = '"+ Tipopago +"' and fecha = '"+ fecha +"';",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getDouble(0);
            } while (c.moveToNext());
        }
        return mont;
    }

    public  double NotasCredito(Context context, String IdVendedor, String IdCliente, String factura){
        db = helper.getReadableDatabase();
        double mont = 0.00;
        Cursor c = db.rawQuery("SELECT Sum(monto) as Total FROM Cobranza where tipodoc in('DC','RX') and IdVendedor = "+
                IdVendedor +" and idCliente = "+ IdCliente +" and RefFact = "+ factura +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getDouble(0);
            } while (c.moveToNext());
        }
        return mont;
    }

    public  double Anticipos(Context context, String IdVendedor, String IdCliente, String factura){
        db = helper.getReadableDatabase();
        double mont = 0.00;
        Cursor c = db.rawQuery("SELECT Sum(monto) as Total FROM Cobranza where tipodoc in('DZ','AB') and RefFact = "+
                factura+" and IdVendedor = "+ IdVendedor +" and idCliente = "+ IdCliente +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getDouble(0);
            } while (c.moveToNext());
        }


        return mont;
    }

    public  Double PagosxFactura(Context context, String IdVendedor, String IdCliente, String factura){
        db = helper.getReadableDatabase();
        BigDecimal mont = null;
        Cursor c = db.rawQuery("SELECT Sum(monto) as Total FROM pagosFact where IdVendedor = "+ IdVendedor +" and idCliente = "+ IdCliente +" and factura = "+ factura +";",null);
        if (c.moveToFirst()) {
            do {
                mont = new BigDecimal(c.getDouble(0)+"");
            } while (c.moveToNext());
        }
        return Double.parseDouble(mont+"");
    }

    public  Boolean FacturaPendientes(Context context, String IdVendedor, String IdCliente){
        db = helper.getReadableDatabase();
        int mont = 0;
        Cursor c = db.rawQuery("SELECT Count(Factura) as nFact FROM Cobranza where IdVendedor = "+ IdVendedor +" and idCliente = "+ IdCliente +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getInt(0);
            } while (c.moveToNext());
        }
        if(mont >=1){
            return true;
        }else{
            return false;
        }
    }

    public  int NumeroPagos(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        int mont = 0;
        Cursor c = db.rawQuery("SELECT Count(documento) as nPagos FROM pagosFact where IdVendedor = "+ IdVendedor +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getInt(0);
            } while (c.moveToNext());
        }
        return mont;
    }

    public  int NumeroFacturas(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        int mont = 0;
        Cursor c = db.rawQuery("SELECT Count(factura) as nPagos FROM pagosFact where IdVendedor = "+ IdVendedor +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getInt(0);
            } while (c.moveToNext());
        }
        return mont;
    }

    public  int NumeroClientes(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        int mont = 0;
        Cursor c = db.rawQuery("SELECT Count(idCliente) as nPagos FROM pagosFact where IdVendedor = "+ IdVendedor +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getInt(0);
            } while (c.moveToNext());
        }
        return mont;
    }

    public  int nClientes(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        int mont = 0;
        Cursor c = db.rawQuery("SELECT Count(_id) as nclientes FROM clientes where Vendedor = "+ IdVendedor +";",null);
        if (c.moveToFirst()) {
            do {
                mont = c.getInt(0);
            } while (c.moveToNext());
        }
        return mont;
    }

    public  ArrayList<Cobranza> cargarCursorCobranza(Context context, String IdVendedor, String IdCliente, String Clase){
        db = helper.getReadableDatabase();
        ArrayList<Cobranza> lis = new ArrayList<Cobranza>();
        Cursor c = db.rawQuery("SELECT * FROM Cobranza where IdVendedor = "+IdVendedor+" and idCliente = "+IdCliente+ " and tipodoc in("+Clase+");",null);
        if (c.moveToFirst()) {
            do {
               lis.add(new Cobranza(Long.parseLong(c.getString(0)),Long.parseLong(c.getString(1)),Long.parseLong(c.getString(2)),c.getString(3),c.getString(4),c.getDouble(5),c.getString(6)));
            } while (c.moveToNext());
        }
        return lis;
    }
    public String conversion(double valor)
    {
        Locale.setDefault(Locale.getDefault());
        DecimalFormat num = new DecimalFormat("#,###.00");
        return num.format(valor);
    }
    public ArrayList<Clientes> cargarCursorClientes(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        ArrayList<Clientes> lis = new ArrayList<Clientes>();
        Cursor c = db.rawQuery("SELECT * FROM clientes where vendedor = "+IdVendedor+";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Clientes(Long.parseLong(c.getString(0)),c.getString(1),c.getString(2),c.getString(3),
                       c.getString(4),c.getString(5),c.getString(6),c.getString(7),c.getString(8),c.getString(9),c.getString(10),c.getString(11)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public ArrayList<Clientes> ClientesConFact(Context context, String IdVendedor){
        db = helper.getReadableDatabase();
        ArrayList<Clientes> lis = new ArrayList<Clientes>();
        String Query ="SELECT Clientes._id, Clientes.Nombre, Clientes.RIF, Clientes.Direccion, Clientes.Vendedor, Clientes.Cupo, Clientes.Canal,Clientes.CondPago, Clientes.Barrio, Clientes.Lista, Clientes.Plazo, Clientes.Activo " +
                      "FROM clientes, Cobranza " +
                      "where Clientes.Vendedor = Cobranza.idVendedor " +
                      "and Clientes._id = Cobranza.idCliente " +
                      "and Clientes.Vendedor = "+IdVendedor+
                      " and tipodoc in('RV','DD') " +
                      " GROUP BY Clientes._id, Clientes.Nombre, Clientes.RIF, Clientes.Direccion, Clientes.Vendedor, Clientes.Cupo, Clientes.Canal,Clientes.CondPago, Clientes.Barrio, Clientes.Lista, Clientes.Plazo, Clientes.Activo;";
        Cursor c = db.rawQuery(Query,null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Clientes(Long.parseLong(c.getString(0)),c.getString(1),c.getString(2),c.getString(3),
                        c.getString(4),c.getString(5),c.getString(6),c.getString(7),c.getString(8),c.getString(9),c.getString(10),c.getString(11)));
            } while (c.moveToNext());
        }
        return lis;
    }
    public ArrayList<Bancos> cargarCursorBancos(Context context,String Tipo){
        int band=0;
        db = helper.getReadableDatabase();
        ArrayList<Bancos> lis = new ArrayList<Bancos>();
        Cursor c = db.rawQuery("SELECT * FROM bancos where tipo = '"+Tipo+"';",null);
        if (c.moveToFirst()) {
            do {
                if (band==0){
                    if (Tipo == "E"){
                        lis.add(new Bancos(Tipo.substring(0,1)+"000", "Seleccione Banco Emisor" ,Tipo));
                        band = 1;
                    }
                    if (Tipo == "R"){
                        lis.add(new Bancos(Tipo.substring(0,1)+"000", "Seleccione Banco Receptor" ,Tipo));
                        band = 1;
                    }
                }
                lis.add(new Bancos(c.getString(0),c.getString(1),c.getString(2)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public ArrayList<Usuario> cargarCursorUsuario(Context context,long lgId, String lgpass){
        db = helper.getReadableDatabase();
        ArrayList<Usuario> lis = new ArrayList<Usuario>();
        Cursor c = db.rawQuery("SELECT * FROM usuario where lgid = "+lgId+" and lgpass = "+lgpass+";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Usuario(Long.parseLong(c.getString(0)),c.getString(1),c.getString(2)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public ArrayList<Usuario> cargarCursorUsuarioS(Context context,long lgId){
        db = helper.getReadableDatabase();
        ArrayList<Usuario> lis = new ArrayList<Usuario>();
        Cursor c = db.rawQuery("SELECT * FROM usuario where lgid = "+lgId+";",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Usuario(Long.parseLong(c.getString(0)),c.getString(1),c.getString(2)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public ArrayList<Usuario> cargarCursorUsuarioV(Context context){
        db = helper.getReadableDatabase();
        ArrayList<Usuario> lis = new ArrayList<Usuario>();
        Cursor c = db.rawQuery("SELECT * FROM usuario where lgpass <> '';",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new Usuario(Long.parseLong(c.getString(0)),c.getString(1),c.getString(2)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public ArrayList<UsuarioA> cargarCursorUsuarioA(Context context){
        db = helper.getReadableDatabase();
        ArrayList<UsuarioA> lis = new ArrayList<UsuarioA>();
        Cursor c = db.rawQuery("SELECT * FROM UsuarioActivo;",null);
        if (c.moveToFirst()) {
            do {
                lis.add(new UsuarioA(Long.parseLong(c.getString(0)),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5)));
            } while (c.moveToNext());
        }
        return lis;
    }

    public void UpdateCursorUsuarioA(Context context, String IdVendedor, String IdCliente, String factura, String tipodoc, String fechafact, String montof){
        String where = "lgId = '" + IdVendedor + "'";
        ContentValues valores = new ContentValues();

        valores.put("IdCliente",IdCliente);
        valores.put("factura",factura);
        valores.put("tipodoc",tipodoc);
        valores.put("fechafact",fechafact);
        valores.put("montof",montof);

        db.update(TABLE_NAMEua, valores, where, null);
        db.close();

    }

    public void UpdateDocPago(Context context, String idpago, String idVendedor, String documento, String bancor, String Tipo){

        if(Tipo == "F"){
            String where = "IdVendedor = '" + idVendedor + "'" + " and idpago = '" + idpago + "'" ;
            ContentValues valores = new ContentValues();

            valores.put("deposito",documento);
            valores.put("bancor",bancor);

            db.update(TABLE_NAMEpf, valores, where, null);
        }else{
            String where = "IdVendedor = '" + idVendedor + "'" + " and idanticipo = '" + idpago + "'" ;
            ContentValues valores = new ContentValues();

            valores.put("deposito",documento);
            valores.put("bancor",bancor);

            db.update(TABLE_NAMEac, valores, where, null);
        }

        db.close();

    }

}
