package com.agronivar.android.appventaslocal;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class ListaClientes extends AppCompatActivity {
    InputStream inputStream;
    String[] ids;
    private Cursor cursor;
    private ListView list;
    ArrayList<Clientes> lisC;
    ArrayList<UsuarioA> lisU;
    public ItemClientes datos[];
    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;
    public TextView cod;
    Button btntodos;
    Button btnfact;
    Button rCobranza;
    Button mpagos;
    Button btnsinc;
    Button btnsalir;
    TextView txtbusq;
    SearchView searchview;
    AdaptadorClientes adaptador;
       @SuppressLint("WrongViewCast")
       @Override
    protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);
           setContentView(R.layout.activity_lista_clientes);
           list = (ListView) findViewById(R.id.lista);
           list.setTextFilterEnabled(true);
           searchview = (SearchView) findViewById(R.id.idCliente);
           registerForContextMenu(list);
           list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
               @Override
               public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                   cod = (TextView) view.findViewById(R.id.idCliente);
                   DbManager manager = new DbManager(getApplicationContext());
                   manager.UpdateCursorUsuarioA(getApplicationContext(),lisU.get(0).getLgId()+"",cod.getText().toString()," "," "," ","0.00");
                   Intent k = new Intent(getApplicationContext(), Cobranzas.class);
                   startActivity(k);
               }
           });
           CargarClientes();

           mpagos = (Button) findViewById(R.id.mpagos);
           mpagos.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent k = new Intent(getApplicationContext(), ListaPagos.class);
                   startActivity(k);
               }
           });
           btntodos = (Button) findViewById(R.id.btntodos);
           btntodos.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   CargarClientes();
               }
           });


           btnfact = (Button) findViewById(R.id.btnfact);
           btnfact.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   CargarClientesConFacturas();
               }
           });

           rCobranza = (Button) findViewById(R.id.rCobranza);
           rCobranza.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent k = new Intent(getApplicationContext(), ResumenCobranza.class);
                   startActivity(k);
               }
           });
           btnsinc = (Button) findViewById(R.id.btnsinc);
           btnsinc.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Sincronizar.Botones("INT");
                   Intent k = new Intent(getApplicationContext(), Sincronizar.class);
                   startActivity(k);
                   finish();
               }
           });

           btnsalir = (Button) findViewById(R.id.btnsalir);
           btnsalir.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   finish();
               }
           });
       }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cob,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
           AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            DbManager manager = new DbManager(getApplicationContext());
           switch (item.getItemId()){
               case R.id.menuCob:
                   cod = (TextView) info.targetView.findViewById(R.id.idCliente);
                   manager.UpdateCursorUsuarioA(getApplicationContext(),lisU.get(0).getLgId()+"",cod.getText().toString()," "," "," ","0.00");
                   Intent k = new Intent(getApplicationContext(), Cobranzas.class);
                   startActivity(k);
                   break;
               case  R.id.menuAnt:
                   cod = (TextView) info.targetView.findViewById(R.id.idCliente);
                   manager.UpdateCursorUsuarioA(getApplicationContext(),lisU.get(0).getLgId()+"",cod.getText().toString()," "," "," ","0.00");
                   Intent A = new Intent(getApplicationContext(), Actanticipos.class);
                   startActivity(A);
                   break;

           }
        return super.onContextItemSelected(item);
    }

    private void startDownload() {
//        String url = "http://192.168.162.245:8080/ServicioWeb/clientes.txt";
        String url = "http://www.agronivar.com/ServidorWeb/clientes.txt";
        new DownloadFileAsync().execute(url);
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Descargando..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    class DownloadFileAsync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }
        @Override
        protected String doInBackground(String... aurl) {
            int count;

            try {
                URL url = new URL(aurl[0]);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                File file = getFilesDir();
                OutputStream output = new FileOutputStream(getFilesDir()+"/clientes.txt");
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {}
            return null;
        }
        protected void onProgressUpdate(String... progress) {
            mProgressDialog.setProgress(Integer.parseInt(progress[0]));
        }
        @Override
        protected void onPostExecute(String unused) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
        }
    }


    public void leerArchivo(){

        File dir = getFilesDir();
        File file = new File(dir,"/clientes.txt");
        DbManager manager = new DbManager(this);


        if(file.exists())
        {
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                Toast.makeText(this, "Iniciando Proceso...", Toast.LENGTH_SHORT).show();
                while ((line = br.readLine()) != null) {
                    ids = line.split(";");

                    manager.insertar(ids[0],ids[1],ids[2],ids[3],ids[4],ids[5],ids[6],ids[7],ids[8],ids[9],ids[10],ids[11]);
                }
                Toast.makeText(this, "Proceso Terminado...", Toast.LENGTH_SHORT).show();
                lisU = manager.cargarCursorUsuarioA(getApplicationContext());
                lisC = manager.cargarCursorClientes(this,lisU.get(0).getLgId()+"");

                ItemClientes datos2[] =new ItemClientes[lisC.size()];
                for(int x=0;x<lisC.size();x++) {
                    datos2[x] = new ItemClientes(R.drawable.deuda,lisC.get(x).getNombre(), Long.toString(lisC.get(x).getIdCliente()));
                }

                datos=datos2;

                AdaptadorClientes adaptador = new AdaptadorClientes(this,datos);

                list.setAdapter(adaptador);
            }
            catch (IOException e) {
            }
        }else{
            Toast.makeText(this, "Archivo no Existe", Toast.LENGTH_SHORT).show();
        }
    }

    private void CargarClientesConFacturas() {
        list.setAdapter(null);
        DbManager manager = new DbManager(this);
        boolean band;
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        lisC = manager.ClientesConFact (this,lisU.get(0).getLgId()+"");
        ItemClientes datos2[] =new ItemClientes[lisC.size()];
        for(int x=0;x<lisC.size();x++) {
            band= manager.FacturaPendientes(this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"");

            if (band == true){
                datos2[x] = new ItemClientes(R.drawable.deuda,lisC.get(x).getNombre(), Long.toString(lisC.get(x).getIdCliente()));
            }
        }

        datos=datos2;

        adaptador = new AdaptadorClientes(this,datos);

        list.setAdapter(adaptador);

    }

    private void CargarClientes(){
        DbManager manager = new DbManager(this);
        boolean band;
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        lisC = manager.cargarCursorClientes(this,lisU.get(0).getLgId()+"");
        ItemClientes datos2[] =new ItemClientes[lisC.size()];
        for(int x=0;x<lisC.size();x++) {
            band= manager.FacturaPendientes(this,lisU.get(0).getLgId()+"",lisC.get(x).getIdCliente()+"");

                if (band == true){
                    datos2[x] = new ItemClientes(R.drawable.deuda,lisC.get(x).getNombre(), Long.toString(lisC.get(x).getIdCliente()));
                }else{
                    datos2[x] = new ItemClientes(R.drawable.solvente,lisC.get(x).getNombre(), Long.toString(lisC.get(x).getIdCliente()));
                }
        }

        datos=datos2;

        adaptador = new AdaptadorClientes(this,datos);
        
        list.setAdapter(adaptador);
    }
    class AdaptadorClientes extends ArrayAdapter<ItemClientes>{

        public AdaptadorClientes(Context context, ItemClientes[] clientes) {
            super(context, R.layout.itemlistclientes,clientes);
        }
        public View getView(int pos, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.itemlistclientes,null);

            TextView Nombre = (TextView)item.findViewById(R.id.Nombre);
            Nombre.setText(datos[pos].getNombre());

            TextView Codigo = (TextView)item.findViewById(R.id.idCliente);
            Codigo.setText(datos[pos].getCodigo());

            ImageView imagen =(ImageView)item.findViewById(R.id.estatus);
            imagen.setImageResource(datos[pos].getEstatus());

            return (item);
        }
    }
}

