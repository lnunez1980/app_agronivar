package com.agronivar.android.appventaslocal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RegistroActivity extends AppCompatActivity {
    Button btncancelar;
    Button btnagregar;

    TextView txtlgId;
    TextView txtlgpass;
    ArrayList<Usuario> lisU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        txtlgId = (TextView) findViewById(R.id.txtlgId);
        txtlgpass = (TextView) findViewById(R.id.txtlgpass);

        btncancelar = (Button) findViewById(R.id.btncancelar);
        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnagregar = (Button) findViewById(R.id.btnagregar);
        btnagregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean band = true;
                if(txtlgId.getText().toString().trim().isEmpty()){
                    band =false;
                }
                if(txtlgpass.getText().toString().trim().isEmpty()){
                    band =false;
                }
                if(band==true){
                    band = false;
                    DbManager manager = new DbManager(getApplicationContext());
                    lisU = manager.cargarCursorUsuarioS(getApplicationContext(),Long.parseLong(txtlgId.getText().toString()));
                    if(lisU.size()==1){
                        lisU=null;
                        manager.updateContraseñaUsuario(Long.parseLong(txtlgId.getText().toString()),txtlgpass.getText().toString());
                        //manager.insertarUsuario(Long.parseLong(txtlgId.getText().toString()),txtlgpass.getText().toString(),txtNombre.getText().toString() );
                        lisU = manager.cargarCursorUsuario(getApplicationContext(),Long.parseLong(txtlgId.getText().toString()),txtlgpass.getText().toString());
                        if(lisU.size()==1){
                            Toast.makeText(getApplicationContext(), "Usuario registrado con exito", Toast.LENGTH_SHORT).show();
                            Intent k = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(k);
                            finish();
                        }else{
                            Toast.makeText(getApplicationContext(), "Codigo no Autorizado...", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else{
                    band = false;
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean isNullOrEmpty(String string){
        return TextUtils.isEmpty(string);
    }
}
