package com.agronivar.android.appventaslocal;

import android.app.Application;

/**
 * Created by lnuñez on 02/11/2017.
 */

public class globalVar extends Application {
private long IdVendedor;
private static String Nombre;
private String pass;

    public long getIdVendedor() {
        return IdVendedor;
    }

    public void setIdVendedor(long idVendedor) {
        IdVendedor = idVendedor;
    }

    public static String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
