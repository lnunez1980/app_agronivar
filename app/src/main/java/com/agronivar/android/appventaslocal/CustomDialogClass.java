package com.agronivar.android.appventaslocal;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by lnuñez on 15/02/2018.
 */

public class CustomDialogClass extends Dialog {

    public Activity c;
    public Dialog d;
    public Button btnmodif, btncancelar;
    public TextView doc;
    public TextView mont;
    public String IdPago;
    public String idVendedor;
    public String documento;
    public Bancos bancosR[];
    public Spinner bancor;
    public ArrayList<Bancos> lisBR;
    public String Codbancor;
    public int Cant;
    public String tipo;
    public CustomDialogClass(Activity a, String IdPago, String idVendedor, String tipo) {
        super(a);
        this.c = a;
        this.IdPago = IdPago;
        this.idVendedor = idVendedor;
        this.tipo = tipo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_editpagos);
        DbManager manager = new DbManager(c);
        doc = (TextView) findViewById(R.id.doc);
        bancor = (Spinner) findViewById(R.id.bancor);
        CargarBancos();
        doc.setText(manager.GetDeposito(c,idVendedor,IdPago));

        int selectionPosition = BuscarPosicion(manager.GetBancor(c,idVendedor,IdPago));
        bancor.setSelection(selectionPosition);

        bancor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView codr = (TextView) view.findViewById(R.id.idCliente);
                Codbancor = codr.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnmodif = (Button) findViewById(R.id.btnmodif);
        btnmodif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DbManager manager = new DbManager(c);
                documento = doc.getText().toString();
                manager.UpdateDocPago(c,IdPago,idVendedor,documento,Codbancor,tipo);
                dismiss();
            }
        });

        btncancelar = (Button) findViewById(R.id.btncancelar);
        btncancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private int BuscarPosicion(String CodBanco){
        //Funcion para llenar lista de Bancos Emisor
        DbManager manager = new DbManager(c);

        //Funcion para llenar lista de Bancos Receptor
        lisBR = manager.cargarCursorBancos(c,"R");
        Cant = lisBR.size();
        Bancos datosR[] = new Bancos[Cant];
        for(int x=0;x<Cant;x++) {
            datosR[x] = new Bancos(lisBR.get(x).getIdbanco(),lisBR.get(x).getNombre(),lisBR.get(x).getTipo());
            if (datosR[x].getIdbanco().equals(CodBanco)){
                return x;
            }
        }
        return 0;
    }

    private void CargarBancos(){
        //Funcion para llenar lista de Bancos Emisor
        DbManager manager = new DbManager(c);

        //Funcion para llenar lista de Bancos Receptor
        lisBR = manager.cargarCursorBancos(c,"R");
        Cant = lisBR.size();
        Bancos datosR[] = new Bancos[Cant];
        for(int x=0;x<Cant;x++) {
            datosR[x] = new Bancos(lisBR.get(x).getIdbanco(),lisBR.get(x).getNombre(),lisBR.get(x).getTipo());
        }
        bancosR=datosR;
        AdaptadorBancosR adaptadorR = new AdaptadorBancosR(c,bancosR);
        bancor.setAdapter(adaptadorR);
    }
    class AdaptadorBancosR extends BaseAdapter {
        Activity activity;
        Bancos[] bancos;
        LayoutInflater inflater;

        //Clase Adaptador para la Lista de Bancos

        public AdaptadorBancosR(Activity activity, Bancos[] bancos) {
            this.activity = activity;
            this.bancos = bancos;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return bancos.length;
        }

        @Nullable
        @Override
        public Bancos getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View Row = inflater.inflate(R.layout.itemlistbancos,null);
            TextView Idbanco = (TextView)Row.findViewById(R.id.idCliente);
            Idbanco.setText(bancosR[i].getIdbanco());

            TextView Nombre = (TextView)Row.findViewById(R.id.Nombre);
            Nombre.setText(bancosR[i].getNombre());

            return Row;
        }
    }

}
