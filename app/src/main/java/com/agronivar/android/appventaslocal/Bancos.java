package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 15/12/2017.
 */

public class Bancos{

   private String Idbanco;
   private String Nombre;
   private String Tipo;

    public Bancos(String idbanco, String nombre, String tipo) {
        Idbanco = idbanco;
        Nombre = nombre;
        Tipo = tipo;
    }

    public String getIdbanco() {
        return Idbanco;
    }

    public void setIdbanco(String idbanco) {
        Idbanco = idbanco;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }
}
