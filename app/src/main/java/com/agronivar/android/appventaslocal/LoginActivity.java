package com.agronivar.android.appventaslocal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    Button btregistrar;
    Button btsalir;
    Button btiniciar;
    Button btnSinc;
    ArrayList<Usuario> lisU;
    TextView txtlgId;
    TextView txtlgpass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
     final DbManager manager = new DbManager(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtlgId = (TextView) findViewById(R.id.txtlgId);
        txtlgpass = (TextView) findViewById(R.id.txtlgpass);


        btregistrar = (Button) findViewById(R.id.btregistrar);
        btregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lisU = manager.cargarCursorUsuarioV(getApplicationContext());
                if(lisU.size()==0){
                       Intent k = new Intent(getApplicationContext(), RegistroActivity.class);
                    startActivity(k);
                    }else{
                        Toast.makeText(getApplicationContext(), "Ya existe un usuario configurado", Toast.LENGTH_SHORT).show();
                    }
                }
        });

        btsalir = (Button) findViewById(R.id.btsalir);
        btsalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btiniciar = (Button) findViewById(R.id.btiniciar);
        btiniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean band = true;
                if(txtlgId.getText().toString().trim().isEmpty()){
                    band =false;
                }
                if(txtlgpass.getText().toString().trim().isEmpty()){
                    band =false;
                }
                if(band==true){
                    lisU = manager.cargarCursorUsuario(getApplicationContext(),Long.parseLong(txtlgId.getText().toString()),txtlgpass.getText().toString());
                    int nclientes = manager.nClientes(LoginActivity.this,txtlgId.getText().toString());
                    if (nclientes > 0){
                        if(lisU.size()==1){
                            Toast.makeText(getApplicationContext(), "Acceso Consedido", Toast.LENGTH_SHORT).show();
                            manager.insertarUsuarioA(Long.parseLong(txtlgId.getText().toString()));
                            Intent k = new Intent(getApplicationContext(), ListaClientes.class);
                            startActivity(k);
                            finish();
                        }else{
                            Toast.makeText(getApplicationContext(), "Verifique Usuario o Contraseña", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Debe Sincronizar... Cobranza ya fue Enviada", Toast.LENGTH_SHORT).show();
                        Sincronizar.Botones("EXT");
                        Intent k = new Intent(getApplicationContext(), Sincronizar.class);
                        startActivity(k);
                        finish();
                    }

                }else{
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnSinc = (Button)findViewById(R.id.btnSinc);
        btnSinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sincronizar.Botones("EXT");
                Intent k = new Intent(getApplicationContext(), Sincronizar.class);
                startActivity(k);

            }
        });
    }


}
