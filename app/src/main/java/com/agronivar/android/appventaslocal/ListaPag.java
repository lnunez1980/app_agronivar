package com.agronivar.android.appventaslocal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class ListaPag extends AppCompatActivity {
    private ListView list;
    TextView idpago;
    String idp[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pag);

        list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaPag.this);

                builder.setTitle("Eliminar Pagos");
                builder.setMessage("Desea Elimianr el Pago Seleccionado?");


                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        idpago = (TextView) view.findViewById(R.id.idCliente);
                        idp = idpago.getText().toString().split(" ");
                        idpago.setText(idp[1]);

                        DbManager manager = new DbManager(getApplicationContext());
                        manager.eliminarPago(idpago.getText().toString());
                        dialog.dismiss();
                    }

                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                AlertDialog alert = builder.create();
                alert.show();
            }
        });



    }
}
