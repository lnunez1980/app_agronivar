package com.agronivar.android.appventaslocal;

/**
 * Created by lnuñez on 11/01/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MyListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<HeaderInfo> deptList;

    public MyListAdapter(Context context, ArrayList<HeaderInfo> deptList) {
        this.context = context;
        this.deptList = deptList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<DetailInfo> pagosList =
                deptList.get(groupPosition).getPagosList();
        return pagosList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        DetailInfo detailInfo = (DetailInfo) getChild(groupPosition, childPosition);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.itemresumen, null);
        }

        TextView idpago = (TextView) view.findViewById(R.id.idCliente);
        idpago.setText(detailInfo.getIdpago().trim());

        TextView Factura = (TextView) view.findViewById(R.id.Factura);
        Factura.setText(detailInfo.getFactura().trim());

        TextView IdCliente = (TextView) view.findViewById(R.id.IdFactura);
        IdCliente.setText(detailInfo.getIdCliente().trim());

        TextView nombCliente = (TextView) view.findViewById(R.id.nombCliente);
        nombCliente.setText(detailInfo.getNombCliente().trim());

        TextView monto = (TextView) view.findViewById(R.id.montoa);
        monto.setText(detailInfo.getMontot().trim());

        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<DetailInfo> pagosList =
                deptList.get(groupPosition).getPagosList();
        return pagosList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return deptList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return deptList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        HeaderInfo headerInfo = (HeaderInfo) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.itemgroup, null);
        }

        TextView descricion = (TextView) view.findViewById(R.id.descricion);
        descricion.setText(headerInfo.getDescripcion().trim());

        TextView montot = (TextView) view.findViewById(R.id.montoa);
        montot.setText(headerInfo.getMontot().trim());

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}