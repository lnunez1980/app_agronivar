package com.agronivar.android.appventaslocal;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Actanticipos extends AppCompatActivity {
    Button btnfecha;
    Button btnagregar;
    Button btncancelar;
    Button btnMenu;
    String tipopago;
    TextView txtFechaV;
    private ListView list;
    ArrayList<UsuarioA> lisU;
    ArrayList<Anticipos> lisC;
    TextView documento;
    TextView monto;
    TextView txttpago;
    Spinner bancoe;
    Spinner bancor;
    public Bancos bancosE[];
    public Bancos bancosR[];
    ArrayList<Bancos> lisBE;
    ArrayList<Bancos> lisBR;
    String idp[];
    TextView idAnticipo;
    Locale locale;
    NumberFormat nf;
    String Codbancoe;
    String Codbancor;
    Boolean band = true;
    public ItemAnticipos datos[];
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actanticipos);

        // configuracion de Decimales en variavle de formato numerico

        locale = new Locale("es","VE");
        nf = NumberFormat.getNumberInstance(locale);

        if (nf instanceof DecimalFormat) {
            ((DecimalFormat)nf).setDecimalSeparatorAlwaysShown(true);
            ((DecimalFormat)nf).setMinimumFractionDigits(2);
        }


        // Menu Forma de pago
        tipopago = "Efectivo";
        btnMenu = (Button) findViewById(R.id.btnMenu);
        registerForContextMenu(btnMenu);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openContextMenu(btnMenu);
            }
        });

        documento = (TextView) findViewById(R.id.documento);
        documento.setEnabled(false);
        monto = (TextView) findViewById(R.id.montoa);
        txtFechaV = (TextView) findViewById(R.id.txtFechaV);
        txttpago = (TextView) findViewById(R.id.txttpago);
        txttpago.setText("Efectivo");
        list = (ListView) findViewById(R.id.listpagos);

        bancoe = (Spinner) findViewById(R.id.bancoe);
        bancor = (Spinner) findViewById(R.id.bancor);
        bancoe.setEnabled(false);
        bancor.setEnabled(false);


        bancoe .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView code = (TextView) view.findViewById(R.id.idCliente);
                Codbancoe = code.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bancor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView codr = (TextView) view.findViewById(R.id.idCliente);
                Codbancor = codr.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // llenar lista de anticipos

        MostrarAnticiposClientes();

        // Item Clic Lista Pagos
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Actanticipos.this);

                builder.setTitle("Eliminar Pagos");
                builder.setMessage("Desea Elimianr el Pago Seleccionado?");


                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        idAnticipo = (TextView) view.findViewById(R.id.idCliente);
                        idp =idAnticipo.getText().toString().split(" ");
                        idAnticipo.setText(idp[1]);

                        DbManager manager = new DbManager(getApplicationContext());
                        manager.eliminarAnticipo(idAnticipo.getText().toString());
                        MostrarAnticiposClientes();
                        dialog.dismiss();
                    }

                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        // Cargar Listas Bancos
        CargarBancos();

        //Asignar Fecha Actual
        Date myDate = new Date();
        txtFechaV.setText(new SimpleDateFormat("dd-MM-yyyy").format(myDate));

        // Menu Forma de pago
        tipopago = "Efectivo";
        btnMenu = (Button) findViewById(R.id.btnMenu);
        registerForContextMenu(btnMenu);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openContextMenu(btnMenu);
            }
        });


        // Boton Mostrar Popup Fecha
        btnfecha = (Button) findViewById(R.id.btnfecha);
        btnfecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int ano = cal.get(Calendar.YEAR);
                int mes = cal.get(Calendar.MONTH);
                int dia = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        Actanticipos.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        ano,mes,dia);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                String fecha;
                mes = mes + 1;
                fecha = dia + "-" + mes + "-" + ano;
                if (mes < 10){
                    fecha = dia + "-" + "0"+mes + "-" + ano;
                }
                if (dia < 10){
                    fecha = "0" + dia + "-" + mes + "-" + ano;
                }
            }
        };

        // Boton Agregar Pago
        btnagregar = (Button) findViewById(R.id.btnagregar);
        btnagregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tipopago.trim().isEmpty()){
                    band =false;
                }
                if(monto.getText().toString().trim().isEmpty()){
                    band =false;
                }
                if((documento.getText().toString().trim().isEmpty()) && (tipopago != "Efectivo")){
                    band =false;
                }
                if ((bancoe.getSelectedItemPosition()==0) && (tipopago != "Efectivo")){
                    band =false;
                    if ((bancoe.getSelectedItemPosition()==0) && (tipopago == "Deposito")){
                        band =true;
                    }
                }
                if ((bancor.getSelectedItemPosition()==0)&& (tipopago != "Efectivo")){
                    band =false;
                    if ((bancor.getSelectedItemPosition()==0) && (tipopago == "Cheque")){
                        band = true;
                    }
                }
                if(band==true){
                    InsertarMostrarPagos();
                    ressetcampos();

                }else{
                    Toast.makeText(getApplicationContext(), "Debe llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void ressetcampos(){
        Date myDate = new Date();
        band = true;
        txtFechaV.setText(new SimpleDateFormat("dd-MM-yyyy").format(myDate));
        txttpago.setText("Efectivo");
        documento.setEnabled(false);
        bancoe.setSelection(0);
        bancor.setSelection(0);
        documento.setText("");


    }
        // Funcion Agregar Anticipos

    private void InsertarMostrarPagos(){

        //Funcion para llenar lista de pagos por factura


        DbManager manager = new DbManager(getApplicationContext());
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        manager.insertarAnticipos(lisU.get(0).getLgId(),Long.parseLong(lisU.get(0).getIdCliente())
                ,documento.getText().toString(),tipopago,Double.parseDouble(monto.getText().toString()),txtFechaV.getText().toString(),Codbancoe,Codbancor);

        lisC = manager.cargarCursorAnticipos (getApplicationContext(),lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"");
        ItemAnticipos datos2[] = new ItemAnticipos[lisC.size()];
        for(int x=0;x<lisC.size();x++) {
            datos2[x] = new ItemAnticipos(lisC.get(x).getIdanticipo(),lisC.get(x).getIdCliente()+"",lisC.get(x).getFecha(), lisC.get(x).getTipopago(),lisC.get(x).getMonto() );
        }
        datos=datos2;
        AdaptadorAnticipos adaptador = new AdaptadorAnticipos(getApplicationContext(),datos);
        list.setAdapter(adaptador);

    }

    // Funcion Cargar Anticipos

    private void MostrarAnticiposClientes(){

        //Funcion para llenar lista de Anticipos


        DbManager manager = new DbManager(getApplicationContext());
        lisU = manager.cargarCursorUsuarioA(getApplicationContext());
        lisC = manager.cargarCursorAnticipos (getApplicationContext(),lisU.get(0).getLgId()+"",lisU.get(0).getIdCliente()+"");
        ItemAnticipos datos2[] = new ItemAnticipos[lisC.size()];
        for(int x=0;x<lisC.size();x++) {
            datos2[x] = new ItemAnticipos(lisC.get(x).getIdanticipo(),lisC.get(x).getIdCliente()+"",lisC.get(x).getFecha(), lisC.get(x).getTipopago(),lisC.get(x).getMonto() );
        }
        datos=datos2;
        AdaptadorAnticipos adaptador = new AdaptadorAnticipos(getApplicationContext(),datos);
        list.setAdapter(adaptador);

    }

    // Eventos de Menu Contextual Forma de pago
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.listpagos){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_elipago,menu);
        }

        if(v.getId() == R.id.btnMenu){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_tpago,menu);
        }


    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.menuEfectivo:
                tipopago = "Efectivo";
                txttpago.setText(tipopago);
                documento.setEnabled(false);
                bancoe.setEnabled(false);
                bancor.setEnabled(false);
                break;
            case  R.id.menuCheque:
                tipopago = "Cheque";
                txttpago.setText(tipopago);
                documento.setEnabled(true);
                bancoe.setEnabled(true);
                bancor.setEnabled(true);
                break;
            case  R.id.menuDeposito:
                tipopago = "Deposito";
                txttpago.setText(tipopago);
                documento.setEnabled(true);
                bancoe.setEnabled(false);
                bancor.setEnabled(true);
                break;
            case  R.id.menuTransferencia:
                tipopago = "Transferencia";
                txttpago.setText(tipopago);
                documento.setEnabled(true);
                bancoe.setEnabled(true);
                bancor.setEnabled(true);
                break;
            case  R.id.menuEliminar:
                Toast.makeText(getApplicationContext(), "Eliminado", Toast.LENGTH_SHORT).show();
                break;

        }
        return super.onContextItemSelected(item);
    }

    class AdaptadorBancosR extends BaseAdapter {
        Activity activity;
        Bancos[] bancos;
        LayoutInflater inflater;

        //Clase Adaptador para la Lista de Bancos

        public AdaptadorBancosR(Activity activity, Bancos[] bancos) {
            this.activity = activity;
            this.bancos = bancos;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return bancos.length;
        }

        @Nullable
        @Override
        public Bancos getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View Row = inflater.inflate(R.layout.itemlistbancos,null);
            TextView Idbanco = (TextView)Row.findViewById(R.id.idCliente);
            Idbanco.setText(bancosR[i].getIdbanco());

            TextView Nombre = (TextView)Row.findViewById(R.id.Nombre);
            Nombre.setText(bancosR[i].getNombre());

            return Row;
        }
    }

    class AdaptadorBancosE extends BaseAdapter {
        Activity activity;
        Bancos[] bancos;
        LayoutInflater inflater;

        //Clase Adaptador para la Lista de Bancos

        public AdaptadorBancosE(Activity activity, Bancos[] bancos) {
            this.activity = activity;
            this.bancos = bancos;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return bancos.length;
        }

        @Nullable
        @Override
        public Bancos getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View Row = inflater.inflate(R.layout.itemlistbancos,null);
            TextView Idbanco = (TextView)Row.findViewById(R.id.idCliente);
            Idbanco.setText(bancosE[i].getIdbanco());

            TextView Nombre = (TextView)Row.findViewById(R.id.Nombre);
            Nombre.setText(bancosE[i].getNombre());

            return Row;
        }
    }
    class AdaptadorAnticipos extends ArrayAdapter<ItemAnticipos> {

        //Clase Adaptador para la Lista de pagos

        public AdaptadorAnticipos(Context context, ItemAnticipos[] anticipos) {
            super(context, R.layout.itemlistant, anticipos);
        }
        public View getView(int pos, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.itemlistant,null);

            TextView idAnticipo = (TextView)item.findViewById(R.id.idCliente);
            idAnticipo.setText("Cod: "+datos[pos].getIdanticipo());

            TextView Monto = (TextView)item.findViewById(R.id.montoa);
            Monto.setText(nf.format(datos[pos].getMonto()));

            TextView tipoP = (TextView)item.findViewById(R.id.tipopago);
            tipoP.setText(datos[pos].getTipoPago());

            TextView idcliente = (TextView)item.findViewById(R.id.IdFactura);
            idcliente.setText("Cliente: "+datos[pos].getIdcliente());

            return (item);
        }
    }

    private void CargarBancos(){
        int Cant;
        //Funcion para llenar lista de Bancos Emisor
        DbManager manager = new DbManager(Actanticipos.this);
        lisBE = manager.cargarCursorBancos(Actanticipos.this,"E");
        Cant = lisBE.size();
        Bancos datosE[] = new Bancos[Cant];

        for(int x=0;x<Cant;x++) {
            datosE[x] = new Bancos(lisBE.get(x).getIdbanco(),lisBE.get(x).getNombre(),lisBE.get(x).getTipo());
        }
        bancosE=datosE;
        AdaptadorBancosE adaptadorE = new AdaptadorBancosE(Actanticipos.this,bancosE);
        bancoe.setAdapter(adaptadorE);

        //Funcion para llenar lista de Bancos Receptor
        lisBR = manager.cargarCursorBancos(Actanticipos.this,"R");
        Cant = lisBR.size();
        Bancos datosR[] = new Bancos[Cant];
        for(int x=0;x<Cant;x++) {
            datosR[x] = new Bancos(lisBR.get(x).getIdbanco(),lisBR.get(x).getNombre(),lisBR.get(x).getTipo());
        }
        bancosR=datosR;
        AdaptadorBancosR adaptadorR = new AdaptadorBancosR(Actanticipos.this,bancosR);
        bancor.setAdapter(adaptadorR);

    }
}
