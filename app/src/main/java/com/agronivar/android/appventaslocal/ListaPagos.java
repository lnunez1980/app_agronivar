package com.agronivar.android.appventaslocal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class ListaPagos extends AppCompatActivity {
    public ItemPagos datos[];
    private ListView list;
    ArrayList<UsuarioA> lisU;
    ArrayList<pagosFacturas> lisC;
    ArrayList<pagosFacturas> lisA;
    public TextView cod;
    public TextView fact;
    String[] idpago;
    String[] idFact;
    String NombCliente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pagos);
        final DbManager manager = new DbManager(ListaPagos.this);
        list = (ListView) findViewById(R.id.Pagos);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                lisU = manager.cargarCursorUsuarioA(ListaPagos.this);
                cod = (TextView) view.findViewById(R.id.idCliente);
                idpago = cod.getText().toString().split(":");
                fact = (TextView) view.findViewById(R.id.IdFactura);
                idFact = fact.getText().toString().split(":");
                String Tipo;
                if (Long.parseLong(idFact[1])==0){
                    Tipo = "A";
                }else {
                    Tipo = "F";
                }
                CustomDialogClass cdd = new CustomDialogClass(ListaPagos.this,idpago[1],lisU.get(0).getLgId()+"",Tipo);
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                cdd.show();
            }
        });
        MostrarPagos();
    }

    private void MostrarPagos(){

        //Funcion para llenar lista de pagos por factura
        int x;
        DbManager manager = new DbManager(ListaPagos.this);
        lisU = manager.cargarCursorUsuarioA(ListaPagos.this);
        lisC = manager.cargarCursorPagosfactCH(ListaPagos.this,lisU.get(0).getLgId()+"");
        lisA = manager.cargarCursorAnticiposCH(ListaPagos.this,lisU.get(0).getLgId()+"");
        ItemPagos datos2[] = new ItemPagos[lisC.size() + lisA.size()];
        for(x=0;x<lisC.size();x++) {
            String NombCliente = manager.NombreCliente(this,lisC.get(x).getIdCliente()+"");
            //if (lisC.get(x).getTipopago().equals("Cheque")){
                datos2[x] = new ItemPagos(lisC.get(x).getFechaP(), lisC.get(x).getTipopago(),lisC.get(x).getFactura(), (double) lisC.get(x).getMonto(),lisC.get(x).getIdpago(),NombCliente,lisC.get(x).getDocumento());
            //}
        }
        if (lisC.size()==0){
            for(x=0;x<lisA.size();x++) {
                NombCliente = manager.NombreCliente(this,lisA.get(x).getIdCliente()+"");
                datos2[x] = new ItemPagos(lisA.get(x).getFechaP(), lisA.get(x).getTipopago(),0, (double) lisA.get(x).getMonto(),lisA.get(x).getIdpago(),NombCliente,lisA.get(x).getDocumento());
            }
        }else{
            for(x=lisC.size();x<(lisA.size()+lisC.size());x++) {
                NombCliente = manager.NombreCliente(this,lisA.get(x-1).getIdCliente()+"");
                datos2[x] = new ItemPagos(lisA.get(x-1).getFechaP(), lisA.get(x-1).getTipopago(),0, (double) lisA.get(x-1).getMonto(),lisA.get(x-1).getIdpago(),NombCliente,lisA.get(x-1).getDocumento());
            }
        }


        datos=datos2;
        AdaptadorPagos adaptador = new AdaptadorPagos(ListaPagos.this,datos);
        list.setAdapter(adaptador);
    }

    class AdaptadorPagos extends ArrayAdapter<ItemPagos> {

        //Clase Adaptador para la Lista de pagos

        Locale locale = new Locale("es","VE");
        NumberFormat nf = NumberFormat.getNumberInstance(locale);

        public AdaptadorPagos(Context context, ItemPagos[] pagos) {
            super(context, R.layout.itemresumen, pagos);
        }
        public View getView(int pos, View convertView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.itemresumen,null);

            TextView Monto = (TextView)item.findViewById(R.id.montoa);
            Monto.setText(nf.format(datos[pos].getMonto()));

            TextView fecha = (TextView)item.findViewById(R.id.Factura);
            fecha.setText("Fecha: "+datos[pos].Fechap);

            TextView Factura = (TextView)item.findViewById(R.id.IdFactura);
            Factura.setText("Fact:"+datos[pos].getCodigof());

            TextView idpago = (TextView)item.findViewById(R.id.idCliente);
            idpago.setText("Idpago:" + datos[pos].getIdpago());

            TextView nombCliente = (TextView)item.findViewById(R.id.nombCliente);
            nombCliente.setText(datos[pos].getNombreCliente());


            return (item);
        }
    }
}
