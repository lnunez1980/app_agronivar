package com.agronivar.android.appventaslocal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

public class ResumenCobranza extends AppCompatActivity {

    private LinkedHashMap<String, HeaderInfo> mySection = new LinkedHashMap<>();
    private ArrayList<HeaderInfo> SectionList = new ArrayList<>();
    private MyListAdapter listAdapter;
    private ExpandableListView expandableListView;
    ArrayList<pagosFacturas> lisC;
    ArrayList<Anticipos> lisA;
    ArrayList<UsuarioA> lisU;
    Locale locale;
    NumberFormat nf;
    Double TotalGeneral = 0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen_cobranza);

        // configuracion de Decimales en variavle de formato numerico

        locale = new Locale("es","VE");
        nf = NumberFormat.getNumberInstance(locale);

        if (nf instanceof DecimalFormat) {
            ((DecimalFormat)nf).setDecimalSeparatorAlwaysShown(true);
            ((DecimalFormat)nf).setMinimumFractionDigits(2);
        }
        Date myDate = new Date();
        String fecha = (new SimpleDateFormat("dd-MM-yyyy").format(myDate));

        DbManager manager = new DbManager(this);
        lisU = manager.cargarCursorUsuarioA(this);
        lisC = manager.cargarCursorPagos(this,lisU.get(0).getLgId()+"",fecha);
        lisA = manager.ListaAnticipos(this,lisU.get(0).getLgId()+"",fecha);

        for(int x=0;x<lisC.size();x++) {
            String NombCliente = manager.NombreCliente(this,lisC.get(x).getIdCliente()+"");
            String Montot = nf.format(manager.TotalPagosxTipo(this,lisU.get(0).getLgId()+"",lisC.get(x).getTipopago(),fecha))+" Bs.";
            Double Mont = manager.TotalPagosxTipo(this,lisU.get(0).getLgId()+"",lisC.get(x).getTipopago(),fecha);
            String Monto = nf.format(lisC.get(x).getMonto())+" Bs.";
            TotalGeneral = TotalGeneral + lisC.get(x).getMonto();
            addPago("      "+lisC.get(x).getTipopago(),Montot+"","Ref: "+lisC.get(x).getDocumento(),
                    "Factura: "+lisC.get(x).getFactura()+"","Cliente: "+lisC.get(x).getIdCliente()+"",NombCliente,Monto);
        }

        for(int x=0;x<lisA.size();x++) {
            String NombCliente = manager.NombreCliente(this,lisA.get(x).getIdCliente()+"");
            String Monto = nf.format(lisA.get(x).getMonto())+" Bs.";
            TotalGeneral = TotalGeneral + lisA.get(x).getMonto() ;
            addPago("      "+"Anticipo",nf.format(lisA.get(x).getMonto())+" Bs. ","Referencia: "+lisA.get(x).getDocumento(),
                    "","Cliente: "+lisA.get(x).getIdCliente()+"",NombCliente,Monto);
        }

        addPago("      "+"Total General",covertirmonto(TotalGeneral)+" Bs. ","","","","","");
        expandableListView = (ExpandableListView) findViewById(R.id.list);
        listAdapter = new MyListAdapter(ResumenCobranza.this, SectionList);
        expandableListView.setAdapter(listAdapter);

    }
    public static String covertirmonto(double mont){
        return new DecimalFormat("#,##0.00").format(mont);
    }
    private int addPago(String Categoria,String Montot, String idPago, String Factura, String IdCliente, String nombCliente, String montot){

        int groupPosition = 0;

        //check the hash map if the group already exists
        HeaderInfo headerInfo = mySection.get(Categoria);
        //add the group if doesn't exists
        if(headerInfo == null){
            headerInfo = new HeaderInfo();
            headerInfo.setDescripcion(Categoria);
            headerInfo.setMontot(Montot);
            mySection.put(Categoria, headerInfo);
            SectionList.add(headerInfo);
        }

        //get the children for the group
        ArrayList<DetailInfo> pagosList = headerInfo.getPagosList();
        //size of the children list
        int listSize = pagosList.size();
        //add to the counter
        listSize++;

        //create a new child and add that to the group
        DetailInfo detailInfo = new DetailInfo();

        detailInfo.setIdpago(idPago);
        detailInfo.setFactura(Factura);
        detailInfo.setIdCliente(IdCliente);
        detailInfo.setNombCliente(nombCliente);
        detailInfo.setMontot(montot);
        pagosList.add(detailInfo);
        headerInfo.setPagosList(pagosList);

        //find the group position inside the list
        groupPosition = SectionList.indexOf(headerInfo);
        return groupPosition;
    }

}
